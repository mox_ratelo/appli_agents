package app.briconett.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.briconett.R;

import app.briconett.models.Prestation;
import app.briconett.models.Prestation;
import app.briconett.tools.GenericTools;


public class PrestationAdapter extends BaseAdapter {
    private ArrayList<Prestation> items;
    private Context mContext;
    private LayoutInflater inflater;
    private IPrestationListener listener;

    public PrestationAdapter(ArrayList<Prestation> prestation, Context mContext,IPrestationListener listener) {
        this.items = prestation;
        this.mContext = mContext;
        this.listener = listener;
        this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public ArrayList<Prestation> getItems() {
        return items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Prestation getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final Prestation prestation = getItem(position);
        if (prestation != null)
        {
            AutoHolder holder = new AutoHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.item_prestation,null);
                holder.type = (TextView)view.findViewById(R.id.type);
                holder.btn_delete_prestation = (ImageView) view.findViewById(R.id.btn_delete_prestation);
                holder.container_type = (RelativeLayout) view.findViewById(R.id.container_type);
                view.setTag(holder);
            }
            else
            {
                holder = (AutoHolder) view.getTag();
            }
            holder.type.setText(prestation.getName());

            holder.container_type.setVisibility(View.VISIBLE);

           holder.btn_delete_prestation.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                   int id =prestation.getId();
                   items.remove(position);
                   GenericTools.deletItem(id);
                   notifyDataSetChanged();
                   listener.deleteItem(id);

               }
           });
        }
        return  view;

    }
    private static  class AutoHolder
    {
        TextView type;
        RelativeLayout container_type;
        ImageView btn_delete_prestation;

    }

    public interface IPrestationListener {

        void deleteItem(int id);

    }
}
