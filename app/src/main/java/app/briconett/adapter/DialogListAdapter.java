package app.briconett.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import app.briconett.R;
import app.briconett.activity.MainActivity;

import app.briconett.fragment.InterventionFragment;
import app.briconett.models.Prestation;
import app.briconett.models.PrestationItemView;
import app.briconett.tools.GenericTools;

import static app.briconett.activity.MainActivity.prest;

/**
 * Created by Phael on 17/12/2017.
 */

public class DialogListAdapter extends BaseAdapter {
    private ArrayList<Prestation> items;
    private Context mContext;
    private LayoutInflater inflater;
    ICheckBoxListener listener;


    public DialogListAdapter(Context mContext,ArrayList<Prestation> items,ICheckBoxListener listener) {
        this.items = items;
        this.mContext = mContext;
        this.listener = listener;
        this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Prestation getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final Prestation prestationItemView = getItem(position);
        if (prestationItemView != null)
        {
            ViewHolder holder = new ViewHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.item_dialog_list,null);
                holder.checkBoxItem = (CheckBox) view.findViewById(R.id.checkBoxItem);
                view.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) view.getTag();
            }
            if(prestationItemView.isSelected())
            {
                holder.checkBoxItem.setChecked(true);
            }
            else
            {
                holder.checkBoxItem.setChecked(false);
            }
            holder.checkBoxItem.setText(prestationItemView.getName());
            final ViewHolder finalHolder = holder;

            holder.checkBoxItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prestationItemView.isSelected() && prestationItemView.isInList())
                    {
                        finalHolder.checkBoxItem.setChecked(true);
                    }
                    else {
                        boolean isChecked = !prestationItemView.isSelected();
                        prestationItemView.setSelected(isChecked);
                        listener.refreshList();
                    }
                }
            });
            holder.checkBoxItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {

                    }

                }
            });
        }
        return  view;
    }

    private static  class ViewHolder
    {
        CheckBox checkBoxItem;

    }

    public interface ICheckBoxListener {

        void changeState(int position,boolean state);
        void refreshList();
    }
}
