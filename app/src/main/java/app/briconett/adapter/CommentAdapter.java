package app.briconett.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import app.briconett.R;
import app.briconett.activity.MainActivity;

import app.briconett.models.PrestationItemView;

/**
 * Created by Phael on 18/12/2017.
 */

public class CommentAdapter extends BaseAdapter {

    private ArrayList<String> items;
    private Context mContext;
    private LayoutInflater inflater;
    DialogListAdapter.ICheckBoxListener listener;


    public CommentAdapter(Context mContext,ArrayList<String> items) {
        this.items = items;
        this.mContext = mContext;
        this.listener = listener;
        this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public String getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final String comment = getItem(position);
        if (comment != null)
        {
            ViewHolder holder = new ViewHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.item_dialog_comment,null);
                holder.textComment = (TextView) view.findViewById(R.id.textComment);
                view.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) view.getTag();
            }
            holder.textComment.setText(comment);

        }
        return  view;
    }

    private static  class ViewHolder
    {
        TextView textComment;

    }

}
