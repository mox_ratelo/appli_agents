package app.briconett.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.briconett.R;
import app.briconett.models.Prestation;
import app.briconett.models.SelectedPrestation;
import app.briconett.tools.GenericTools;

/**
 * Created by Phael on 12/01/2018.
 */

public class PrestationDetailAdapter extends BaseAdapter {
    private ArrayList<SelectedPrestation> items;
    private Context mContext;
    private LayoutInflater inflater;


    public PrestationDetailAdapter(ArrayList<SelectedPrestation> prestation, Context mContext) {
        this.items = prestation;
        this.mContext = mContext;
        this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public ArrayList<SelectedPrestation> getItems() {
        return items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public SelectedPrestation getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final SelectedPrestation prestation = getItem(position);
        if (prestation != null)
        {
            AutoHolder holder = new AutoHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.item_prestation_detail,null);
                holder.type = (TextView)view.findViewById(R.id.type);
                view.setTag(holder);
            }
            else
            {
                holder = (AutoHolder) view.getTag();
            }
            holder.type.setText(prestation.getNom());


        }
        return  view;

    }
    private static  class AutoHolder
    {
        TextView type;

    }


}
