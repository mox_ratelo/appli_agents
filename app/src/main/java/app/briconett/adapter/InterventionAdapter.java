package app.briconett.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.briconett.R;
import app.briconett.models.EventModel;
import app.briconett.tools.GenericTools;


public class InterventionAdapter extends BaseAdapter{
    private ArrayList<EventModel> interventions;
    private Context mContext;
    private LayoutInflater inflater;

    public InterventionAdapter(ArrayList<EventModel> in, Context mContext) {
        this.interventions = in;
        this.mContext = mContext;
        this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return interventions.size();
    }

    @Override
    public EventModel getItem(int i) {
        return interventions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {


        EventModel interv = getItem(position);

        if (interv != null)
        {
            IntervHolder holder = new IntervHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.item_intervention,null);
                holder.date = (TextView)view.findViewById(R.id.date);
                holder.desc = (TextView)view.findViewById(R.id.desc);
                holder.client = (TextView)view.findViewById(R.id.nomclient);
                holder.item_intervention = (LinearLayout) view.findViewById(R.id.item_intervention);
                holder.heure = (TextView)view.findViewById(R.id.heure);
                view.setTag(holder);
            }
            else
            {
                holder = (IntervHolder) view.getTag();
            }
            // if (Generictools.isNullOrEmpty(emp.getEmail())) {

            if (interv.getDate() != null){
                holder.date.setText(interv.getDate().toString());
            }

            if (interv.getSignature_client() != null){
                holder.client.setText(interv.getSignature_client());
            }

            if (interv.getCommentaire() != null){
                holder.desc.setText(interv.getCommentaire());
            }
            String heure = "";
            if (interv.getHeure_debut() != null) {
                heure = heure+interv.getHeure_debut();
            }
            if (interv.getHeure_fin() != null) {
                heure = heure+" / "+interv.getHeure_fin();
            }
            if (interv.getDureeeffectuee() != null && interv.getDureeprevue()!=null) {
                heure = heure+" ( "+ GenericTools.splitdate(interv.getDureeeffectuee())+" - "+interv.getDureeprevue()+" prévu )";
            }
            holder.heure.setText(heure);
               int dureespre = 0;
               try {
                   if (interv.getDureeprevue() != null) {
                       if (interv.getDureeprevue().contains("30 minutes")) {
                           interv.setDureeprevue("30min");
                       }
                       if (interv.getDureeprevue().split(" ").length == 2) {
                           dureespre = new Integer(interv.getDureeprevue().split(" ")[0].split("h")[0]).intValue() * 3600 +
                                   new Integer(interv.getDureeprevue().split(" ")[1].split("min")[0]).intValue() * 60;
                       } else
                           dureespre = new Integer(interv.getDureeprevue().split("min")[0]).intValue() * 60;
                       String dureeef = GenericTools.splitdate(interv.getDureeeffectuee());
                       int dureeeff = 0;
                       if (dureeef.split(" ").length == 2) {
                           if ((dureeef.split(" ")[1]).contains("sec")) {
                               dureeeff = new Integer(dureeef.split(" ")[0].split("min")[0]).intValue() * 60 +
                                       new Integer(dureeef.split(" ")[1].split("sec")[0]).intValue();
                           } else
                               dureeeff = new Integer(dureeef.split(" ")[0].split("min")[0]).intValue() * 60 +
                                       new Integer(dureeef.split(" ")[1]).intValue();
                       } else if (dureeef.split(" ").length == 3) {
                           if ((dureeef.split(" ")[2]).contains("sec")) {
                               dureeeff = new Integer(dureeef.split(" ")[0].split("min")[0]).intValue() * 60 +
                                       new Integer(dureeef.split(" ")[2].split("sec")[0]).intValue();
                           } else
                               dureeeff = new Integer(dureeef.split(" ")[0].split("min")[0]).intValue() * 60 +
                                       new Integer(dureeef.split(" ")[2]).intValue();
                       } else dureeeff = new Integer(dureeef.split("sec")[0]).intValue();
                       // GenericTools.dureeEnSecondes(GenericTools.splitdate(interv.getDureeeffectuee()));
                       if (dureeeff * 100 / dureespre > 80) {
                           holder.item_intervention.setBackgroundColor(ContextCompat.getColor(mContext, R.color.vert_deau));
                       } else if (dureeeff * 100 / dureespre < 20) {
                           holder.item_intervention.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_grenadine));
                       } else {
                           holder.item_intervention.setBackgroundColor(Color.LTGRAY);
                       }
                   }
               }
               catch (Exception e){
                   holder.item_intervention.setBackgroundColor(Color.LTGRAY);
        }


            }

        return  view;
    }



    private static  class IntervHolder
    {
        TextView title;
        TextView date;
        TextView desc;
        TextView client;
        TextView chrono;
        LinearLayout item_intervention;
        TextView debut;
        TextView fin;
        TextView heure;

    }
}
