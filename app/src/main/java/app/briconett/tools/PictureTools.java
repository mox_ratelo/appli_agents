package app.briconett.tools;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Phael on 14/12/2017.
 */

public class PictureTools {
    private static final String TAG = PictureTools.class.getName();


    static String resizePicture(String pathFile, String fileName) throws IOException {
        try {

            int PICTURE_SIZE = 320;
            Bitmap bitmapDecoded = GenericTools.decodeSampledBitmapFromUri(pathFile, PICTURE_SIZE, PICTURE_SIZE);

            Bitmap bitmap = GenericTools.rotateImage(bitmapDecoded,pathFile);

            if(bitmap==null)
                bitmap = bitmapDecoded;

            pathFile = saveBitmap(bitmap,fileName);

        }catch (OutOfMemoryError outOfMemoryError){

            outOfMemoryError.printStackTrace();
        }
        return pathFile;
    }


    public static String saveBitmap(Bitmap bmp, String filename) throws IOException {

        String newFilePath = "";

        if (bmp!=null) {

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            if(filename.contains(".png")){
                bmp.compress(Bitmap.CompressFormat.PNG, 100, bytes);
            }else{
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            }

            File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+File.separator+ Constant.PICTURE_DIRECTORY,filename);

            boolean newFile = f.createNewFile();
            GenericTools.Log(TAG, "create new file = " + newFile);

            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
            newFilePath = f.getAbsolutePath();
        }

        return newFilePath;

    }


    /** Create a file Uri for saving an image or video */
    public static Uri getOutputMediaFileUri(File file){
        return Uri.fromFile(file);
    }

    /** Create a File for saving an image or video */

    public static File createFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Constant.PICTURE_DIRECTORY);

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("AGENT", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_AGENT_"+ timeStamp + Constant.JPEG_EXTENSION);
    }

}