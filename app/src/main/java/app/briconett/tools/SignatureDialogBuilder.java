package app.briconett.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.view.View;


import java.io.File;

import app.briconett.R;

public class SignatureDialogBuilder {

    public interface SignatureEventListener {
        public void onSignatureEntered(File savedFile);
        public void onSignatureInputCanceled();
        public void onSignatureInputError(Throwable e);
    }

    public void show(Activity activity, final SignatureEventListener eventListener){
        final View view = buildView(activity);
        final SignatureInputView inputView = (SignatureInputView) view.findViewById(R.id.sig__input_view);
        final View header = activity.getLayoutInflater()
                .inflate(R.layout.signature_header, null, false);


        AlertDialog dialog =  new AlertDialog.Builder(activity, R.style.MyDialogTheme)
                .setCustomTitle(header)
                .setView(view)
                .setPositiveButton(R.string.sig__default_dialog_action_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try{
                            if(!inputView.isSignatureInputAvailable())
                                throw new NoSignatureException("No signature found");

                            File saved = inputView.saveSignature();

                            eventListener.onSignatureEntered(saved);
                        }
                        catch(Exception e){
                            e.printStackTrace();

                            eventListener.onSignatureInputError(e);
                        }
                    }
                })
                .setNegativeButton(R.string.sig__default_dialog_action_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        eventListener.onSignatureInputCanceled();
                    }
                })
                .show();


        try
        {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = dialog.findViewById(dividerId);
            //divider.setBackgroundColor(activity.getResources().getColor(R.color.red_bretagne));
            divider.setBackgroundColor(activity.getResources().getColor(R.color.white));

        }
        catch (Exception ex)
        {

        }



        view.findViewById(R.id.sig__action_undo)
                .setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        inputView.undoLastSignaturePath();
                    }
                });
    }

    protected View buildView(Activity activity){
        return activity.getLayoutInflater()
                .inflate(R.layout.sig__default_dialog, null, false);
    }


}
