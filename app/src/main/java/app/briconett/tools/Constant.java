package app.briconett.tools;

import android.Manifest;

/**
 * Created by Phael on 14/12/2017.
 */

public class Constant {

    public static String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    public static final int CAMERA_PERMISSIONS = 10;
    public static final int RESULT_GALLERY = 2;
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static String JPEG_EXTENSION = ".jpg";
    public static final int REQUEST_TAKE_PHOTO = 1;
    public static final String PICTURE_DIRECTORY = "AGENT";

    public static final int SIGNATURE_PICTUR_WIDTH = 150;
    public static final int SIGNATURE_PICTUR_HEIGTH = 120;
    public static final String BASE_URL = "https://client-briconett.fr/";

}
