package app.briconett.tools;

import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.IOException;

/**
 * Created by Phael on 14/12/2017.
 */

public class ResizeImageAsyncTask extends AsyncTask<String, Void, String>
{
    private IResizeImageListener listener;

    public ResizeImageAsyncTask(IResizeImageListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {

        String filePathTemp = params[0];
        String fileNameTemp = params[1];

        try {
            String newPathFile = PictureTools.resizePicture(filePathTemp,fileNameTemp);

            if(!TextUtils.isEmpty(newPathFile)){
                filePathTemp = newPathFile;
            }

        } catch (IOException e) {
            e.printStackTrace();
            this.cancel(true);
        }
        return  filePathTemp;
    }


    @Override
    protected void onPostExecute(String filePathTemp)
    {
        if(listener != null) {
            listener.resizeFinished(filePathTemp);
        }
    }



    public interface IResizeImageListener {
        void resizeFinished(String filePath);
    }




}
