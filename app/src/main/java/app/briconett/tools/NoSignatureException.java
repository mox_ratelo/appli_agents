package app.briconett.tools;

public class NoSignatureException extends RuntimeException {

    public NoSignatureException(String value){
        super(value);
    }

}
