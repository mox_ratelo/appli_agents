package app.briconett.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.BuildConfig;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.briconett.R;
import app.briconett.models.CategorieViewModel;
import app.briconett.models.Prestation;

import static app.briconett.tools.Constant.permissions;

/**
 * Created by Phael on 13/12/2017.
 */

public class GenericTools {

    private static final String PREFERENCES = "BRICO_PREFERENCE_USER";
    /**
     * @param string
     * @return true if the string is equal to null or empty ("")
     */
    public static boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0;
    }


    /**
     * Test if the current device is connected to Internet
     * @param context
     * @return true if device is Online
     */
    public static boolean isDeviceOnline(final Context context) {

        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

            if (activeNetwork.getState() ==
                    NetworkInfo.State.CONNECTED ||
                    activeNetwork.getState() ==
                            NetworkInfo.State.CONNECTING) {
                return true;
            } else if (
                    activeNetwork.getState() ==
                            NetworkInfo.State.DISCONNECTED ||
                            activeNetwork.getState() ==
                                    NetworkInfo.State.DISCONNECTED) {
                return false;
            }

        } catch (NullPointerException ex) {
            return false;
        }

        return false;

    }

    /**
     * VERBOSE and DEBUG logs must be disabled on release builds
     * @param tag
     * @param message
     */
    public static void Log(String tag, String message){
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }

    /**
     * Test if a String is a valid email
     * @param email
     * @return
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    public static Bitmap drawableToBitmap (Drawable drawable) {

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static boolean checkPermissions(Activity activity) {
        int result;
        boolean isCheck;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 10);
            isCheck = false;
        } else {
            isCheck = true;
        }
        return isCheck;
    }

    public static boolean checkPermissions(Activity activity, String[] permissionsList, int permissionCode, Fragment fragment) {
        int result;
        boolean isCheck;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissionsList) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            if (fragment == null)
            {
                ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), permissionCode);
            } else {
                fragment.requestPermissions( listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), permissionCode);
            }
            isCheck = false;
        } else {
            isCheck = true;
        }
        return isCheck;
    }

    public static boolean checkPermissions(Activity activity, String[] permissionsList, int permissionCode) {
        int result;
        boolean isCheck;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissionsList) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {

            isCheck = false;
        } else {
            isCheck = true;
        }
        return isCheck;
    }




    public static Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static Bitmap rotateImage(Bitmap img,String pathFile)
    {
        int orientation;

        try
        {
            ExifInterface exif = new ExifInterface(pathFile);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Log.e("ExifInteface .........", "rotation ="+orientation);

            //exif.setAttribute(ExifInterface.ORIENTATION_ROTATE_90, 90);

            Log.e("orientation", "" + orientation);
            Matrix m = new Matrix();

            if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);
                //m.postScale((float) bm.getWidth(), (float) bm.getHeight());
                // if(m.preRotate(90)){
                Log.e("in orientation", "" + orientation);
                img = Bitmap.createBitmap(img, 0, 0, img.getWidth(),img.getHeight(), m, true);
                return img;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                Log.e("in orientation", "" + orientation);
                img = Bitmap.createBitmap(img, 0, 0, img.getWidth(),img.getHeight(), m, true);
                return img;
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                Log.e("in orientation", "" + orientation);
                img = Bitmap.createBitmap(img, 0, 0, img.getWidth(),img.getHeight(), m, true);
                return img;
            }
//            else if (orientation == ExifInterface.ORIENTATION_UNDEFINED) {
//                m.postRotate(90);
//                Log.e("in orientation", "" + orientation);
//                img = Bitmap.createBitmap(img, 0, 0, img.getWidth(),img.getHeight(), m, true);
//                return img;
//            }
            return img;
        }
        catch (Exception ex)
        {
            Log.e("Exception",ex.getMessage());
            return null;
        }

    }

    public static String getFileNameByFileUrl(String fileUrl) {
        String[] result = fileUrl.split("/");
        String filename="";
        if(result.length > 0){
            filename = result[result.length-1];
        }
        return filename;
    }
    public static void saveString(Context context,String key,String value) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor .putString(key, value);
        prefsEditor.apply();
    }
    public static String getString(Context context,String key) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if (preferences!= null) {
            return preferences.getString(key, "");
        }
        return "";
    }

    public static long getDateDiff(Date date1, Date date2) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return diffInMillies;
//        return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static int dureeEnSecondes(String duree){
        int heure= 0;
        int min  = 0;
        int sec = 0;
        int rep = 0;
        if(duree.split("h")[0].compareTo("00")==0){
            if(duree.split("h")[1].split("m")[0].compareTo("00")==0){
                sec = new Integer(duree.split("h")[1].split("m")[1]);
            }
            else min = new Integer(duree.split("h")[1].split("m")[0]).intValue();
        }
        else if(duree.split("h")[0].compareTo("00")!=0){
            if(duree.split(" ").length==3){
                min = new Integer(duree.split(" ")[0]).intValue();
                sec = new Integer(duree.split(" ")[2]).intValue();
            }
            else if(duree.split(" ").length==2){
                sec = new Integer(duree.split(" ")[0]).intValue();
            }
           else heure = new Integer(duree.split("h")[0]).intValue();
        }
        rep = heure*3600  + min*60 + sec;
        return rep;
    }


    public static void sendMail(Activity activity, String subject,String message) {

        try
        {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:" + "mail@mail.com"));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
            emailIntent.putExtra(Intent.EXTRA_TEXT, message);
            if (emailIntent.resolveActivity(activity.getPackageManager())!=null)
            {
                activity.startActivity(Intent.createChooser(emailIntent, "Send email using..."));
            } else {
                Toast.makeText(activity, activity.getString(R.string.no_app_installed), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
//            e.printStackTrace();
            Toast.makeText(activity, activity.getString(R.string.no_app_installed), Toast.LENGTH_SHORT).show();
        }

    }


    public static boolean imageViewhasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }

        return hasImage;
    }


    public static void adjustListViewHeightDynamically(ListView listView, BaseAdapter baseAdapter){

        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < baseAdapter.getCount(); i++) {
            View listItem = baseAdapter.getView(i, null, vg);
            if(listItem != null)
            {
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }

        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (baseAdapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
        listView.setAdapter(baseAdapter);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView, Context context) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int totalHeight = 0;
        Log.d("WIDTH LIST VIEW",String.valueOf(listView.getWidth()));
        Log.d("EXACTLY",String.valueOf(View.MeasureSpec.EXACTLY));

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            //if < android 4.4
            listItem.setLayoutParams(new ViewGroup.LayoutParams(0,0));
            listItem.measure(size.x, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static File getFileFromImage(Bitmap image, Context context) throws IOException {

        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());

        File storagePath = new File(Environment.getExternalStorageDirectory() + "/Android/data/bretagne/files/");

        if (!storagePath.exists())
            storagePath.mkdirs();

        File pictureFile = new File(storagePath, "img_" + timeStamp + ".jpeg");

        if (pictureFile == null) {
            Log.d("BA",
                    "Error creating media file, check storage permissions: ");
            return null;
        }
        try {

            FileOutputStream fos = new FileOutputStream(pictureFile);

            image.compress(Bitmap.CompressFormat.JPEG, 100, fos);

            fos.close();

        } catch (FileNotFoundException e) {
            Log.d("BA", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("BA", "Error accessing file: " + e.getMessage());
        }

        return pictureFile;

    }

    public static void hiddenKeyboard(Activity activity)
    {
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    public static void deletItem(int id)
    {
        if(AgentSingleton.getInstance().prestationArrayList != null)
        {
            for (Prestation prestation : AgentSingleton.getInstance().prestationArrayList)
            {
                if(prestation.getId() == id)
                {
                    prestation.setSelected(false);
                    prestation.setInList(false);
                }
            }
        }

    }


    public static void reinitPrestationList()
    {
        if(AgentSingleton.getInstance().prestationArrayList != null)
        {
            for (Prestation prestation : AgentSingleton.getInstance().prestationArrayList)
            {
                prestation.setSelected(false);
                prestation.setInList(false);

            }
        }

    }

    public static String splitdate(String diffString){
        if(diffString.split("h")[0].compareTo("00")==0){
            if(diffString.split("h")[1].split("m")[0].compareTo("00")==0){
                diffString = diffString.split("h")[1].split("m")[1]+"sec";
            }
            else {

                diffString = diffString.split("h")[1].split("m")[0]+"min "+diffString.split("h")[1].split("m")[1]+"";
            }
        }
        return diffString;
    }


}
