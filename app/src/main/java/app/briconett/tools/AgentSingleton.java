package app.briconett.tools;

import android.app.Activity;

import java.util.ArrayList;
import java.util.Date;

import app.briconett.models.CategorieViewModel;
import app.briconett.models.Client;
import app.briconett.models.EventModel;
import app.briconett.models.Intervention;
import app.briconett.models.Prestation;
import app.briconett.models.User;

/**
 * Created by Phael on 26/12/2017.
 */

public class AgentSingleton {

    public User currentUser;
    public Client client;
    public EventModel interv;

    public boolean isEndScanQRC;

    public ArrayList<Client> listClients;
    public ArrayList<Prestation> prestationArrayList ;
    public Date beginDate;

    private static final AgentSingleton ourInstance = new AgentSingleton();

    public String heurrePasser;
    public String heurreEstimer;
    public String debut;
    public String fin;

    public static AgentSingleton getInstance() {
        return ourInstance;
    }

    private AgentSingleton() {
    }

    public Activity activity;

    public boolean isSetting;


}
