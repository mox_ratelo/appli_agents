package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Phael on 07/02/2018.
 */

public class UpdatePasswordResponse implements Parcelable {
    public boolean success;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
    }

    public UpdatePasswordResponse() {
    }

    protected UpdatePasswordResponse(Parcel in) {
        this.success = in.readByte() != 0;
    }

    public static final Creator<UpdatePasswordResponse> CREATOR = new Creator<UpdatePasswordResponse>() {
        @Override
        public UpdatePasswordResponse createFromParcel(Parcel source) {
            return new UpdatePasswordResponse(source);
        }

        @Override
        public UpdatePasswordResponse[] newArray(int size) {
            return new UpdatePasswordResponse[size];
        }
    };
}
