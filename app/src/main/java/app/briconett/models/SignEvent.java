package app.briconett.models;


import android.os.Parcel;
import android.os.Parcelable;

public class SignEvent implements Parcelable {
    private String img_base64;
    private int id;
    private String img_file_name;
    private String url;
    private int evenements_id;
    private int type;

    public String getImg_base64() {
        return img_base64;
    }

    public void setImg_base64(String img_base64) {
        this.img_base64 = img_base64;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_file_name() {
        return img_file_name;
    }

    public void setImg_file_name(String img_file_name) {
        this.img_file_name = img_file_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getEvenements_id() {
        return evenements_id;
    }

    public void setEvenements_id(int evenements_id) {
        this.evenements_id = evenements_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.img_base64);
        dest.writeInt(this.id);
        dest.writeString(this.img_file_name);
        dest.writeString(this.url);
        dest.writeInt(this.evenements_id);
        dest.writeInt(this.type);
    }

    public SignEvent() {
    }

    protected SignEvent(Parcel in) {
        this.img_base64 = in.readString();
        this.id = in.readInt();
        this.img_file_name = in.readString();
        this.url = in.readString();
        this.evenements_id = in.readInt();
        this.type = in.readInt();
    }

    public static final Parcelable.Creator<SignEvent> CREATOR = new Parcelable.Creator<SignEvent>() {
        @Override
        public SignEvent createFromParcel(Parcel source) {
            return new SignEvent(source);
        }

        @Override
        public SignEvent[] newArray(int size) {
            return new SignEvent[size];
        }
    };
}
