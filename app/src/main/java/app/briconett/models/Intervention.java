package app.briconett.models;

import java.util.Date;

public class Intervention {
    private int id;
    private String date;
    private String customer;
    private String duration;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Intervention(String date, String customer, String duration) {
        this.date = date;
        this.customer = customer;
        this.duration = duration;
    }
    public Intervention(){

    }
}
