package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageEvent implements Parcelable {

    private String img_base64;
    private int id;
    private String img_file_name;
    private String url;
    private int evenements_id;

    public static final Creator<ImageEvent> CREATOR = new Creator<ImageEvent>() {
        @Override
        public ImageEvent createFromParcel(Parcel source) {
            ImageEvent var = new ImageEvent();
            var.img_base64 = source.readString();
            var.id = source.readInt();
            var.img_file_name = source.readString();
            var.url = source.readString();
            var.evenements_id = source.readInt();
            return var;
        }

        @Override
        public ImageEvent[] newArray(int size) {
            return new ImageEvent[size];
        }
    };


    public String getImg_base64() {
        return this.img_base64;
    }

    public void setImg_base64(String img_base64) {
        this.img_base64 = img_base64;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_file_name() {
        return this.img_file_name;
    }

    public void setImg_file_name(String img_file_name) {
        this.img_file_name = img_file_name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getEvenements_id() {
        return this.evenements_id;
    }

    public void setEvenements_id(int evenements_id) {
        this.evenements_id = evenements_id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.img_base64);
        dest.writeInt(this.id);
        dest.writeString(this.img_file_name);
        dest.writeString(this.url);
        dest.writeInt(this.evenements_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
