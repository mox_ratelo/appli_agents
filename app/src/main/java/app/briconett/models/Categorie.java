package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Categorie implements Parcelable {

    private int id;
    private String nom;

    public static final Creator<Categorie> CREATOR = new Creator<Categorie>() {
        @Override
        public Categorie createFromParcel(Parcel source) {
            Categorie var = new Categorie();
            var.id = source.readInt();
            var.nom = source.readString();
            return var;
        }

        @Override
        public Categorie[] newArray(int size) {
            return new Categorie[size];
        }
    };


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
