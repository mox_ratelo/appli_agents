package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

public class Client implements Parcelable {

    private int role;
    private boolean isrelance;
    private String commentaire_preference;
    private String qrcode;
    private String email_secondaire;
    private String nom;
    private String password;
    private boolean is_mpd_update;
    private String sage;
    private Object modified;
    private String tel;
    private int id;
    private String email;
    private Object longitude;
    private String ville;
    private String contact_place;
    private int condition_reglements_id;
    private String nom_interne;
    private Object lattitude;
    private String created;
    private String cp;
    private String wonett;
    private boolean isfacture;
    private String codeapp;
    private String adresse;
    private int statut;
    private String username;
    private String qrcodeurl;

    public Client(){}

    public Client(int _idClient,String _nomClient, String _email){

        this.id = _idClient;
        this.nom = _nomClient;
        this.email = _email;

    }

    public int getRole() {
        return this.role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public boolean getIsrelance() {
        return this.isrelance;
    }

    public void setIsrelance(boolean isrelance) {
        this.isrelance = isrelance;
    }

    public String getCommentaire_preference() {
        return this.commentaire_preference;
    }

    public void setCommentaire_preference(String commentaire_preference) {
        this.commentaire_preference = commentaire_preference;
    }

    public String getQrcode() {
        return this.qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getEmail_secondaire() {
        return this.email_secondaire;
    }

    public void setEmail_secondaire(String email_secondaire) {
        this.email_secondaire = email_secondaire;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getIs_mpd_update() {
        return this.is_mpd_update;
    }

    public void setIs_mpd_update(boolean is_mpd_update) {
        this.is_mpd_update = is_mpd_update;
    }

    public String getSage() {
        return this.sage;
    }

    public void setSage(String sage) {
        this.sage = sage;
    }

    public Object getModified() {
        return this.modified;
    }

    public void setModified(Object modified) {
        this.modified = modified;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getIdClient() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public String getVille() {
        return this.ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getContact_place() {
        return this.contact_place;
    }

    public void setContact_place(String contact_place) {
        this.contact_place = contact_place;
    }

    public int getCondition_reglements_id() {
        return this.condition_reglements_id;
    }

    public void setCondition_reglements_id(int condition_reglements_id) {
        this.condition_reglements_id = condition_reglements_id;
    }

    public String getNom_interne() {
        return this.nom_interne;
    }

    public void setNom_interne(String nom_interne) {
        this.nom_interne = nom_interne;
    }

    public Object getLattitude() {
        return this.lattitude;
    }

    public void setLattitude(Object lattitude) {
        this.lattitude = lattitude;
    }

    public String getCreated() {
        return this.created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCp() {
        return this.cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getWonett() {
        return this.wonett;
    }

    public void setWonett(String wonett) {
        this.wonett = wonett;
    }

    public boolean getIsfacture() {
        return this.isfacture;
    }

    public void setIsfacture(boolean isfacture) {
        this.isfacture = isfacture;
    }

    public String getCodeapp() {
        return this.codeapp;
    }

    public void setCodeapp(String codeapp) {
        this.codeapp = codeapp;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getStatut() {
        return this.statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getQrcodeurl() {
        return this.qrcodeurl;
    }

    public void setQrcodeurl(String qrcodeurl) {
        this.qrcodeurl = qrcodeurl;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.role);
        dest.writeByte(isrelance ? (byte) 1 : (byte) 0);
        dest.writeString(this.commentaire_preference);
        dest.writeString(this.qrcode);
        dest.writeString(this.email_secondaire);
        dest.writeString(this.nom);
        dest.writeString(this.password);
        dest.writeByte(is_mpd_update ? (byte) 1 : (byte) 0);
        dest.writeString(this.sage);
        dest.writeString(this.tel);
        dest.writeInt(this.id);
        dest.writeString(this.email);
        dest.writeString(this.ville);
        dest.writeString(this.contact_place);
        dest.writeInt(this.condition_reglements_id);
        dest.writeString(this.nom_interne);
        dest.writeString(this.created);
        dest.writeString(this.cp);
        dest.writeString(this.wonett);
        dest.writeByte(isfacture ? (byte) 1 : (byte) 0);
        dest.writeString(this.codeapp);
        dest.writeString(this.adresse);
        dest.writeInt(this.statut);
        dest.writeString(this.username);
        dest.writeString(this.qrcodeurl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Client> CREATOR = new Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel source) {
            Client var = new Client();
            var.role = source.readInt();
            var.isrelance = source.readByte() != 0;
            var.commentaire_preference = source.readString();
            var.qrcode = source.readString();
            var.email_secondaire = source.readString();
            var.nom = source.readString();
            var.password = source.readString();
            var.is_mpd_update = source.readByte() != 0;
            var.sage = source.readString();
            var.modified = source.readParcelable(Object.class.getClassLoader());
            var.tel = source.readString();
            var.id = source.readInt();
            var.email = source.readString();
            var.longitude = source.readParcelable(Object.class.getClassLoader());
            var.ville = source.readString();
            var.contact_place = source.readString();
            var.condition_reglements_id = source.readInt();
            var.nom_interne = source.readString();
            var.lattitude = source.readParcelable(Object.class.getClassLoader());
            var.created = source.readString();
            var.cp = source.readString();
            var.wonett = source.readString();
            var.isfacture = source.readByte() != 0;
            var.codeapp = source.readString();
            var.adresse = source.readString();
            var.statut = source.readInt();
            var.username = source.readString();
            var.qrcodeurl = source.readString();
            return var;
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };

}
