package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

public class SelectedPrestation implements Parcelable {
    public static final Creator<SelectedPrestation> CREATOR = new Creator<SelectedPrestation>() {
        @Override
        public SelectedPrestation createFromParcel(Parcel source) {
            SelectedPrestation var = new SelectedPrestation();
            var.id = source.readInt();
            var.nom = source.readString();
            var.categorie_prestations_id = source.readInt();
            return var;
        }

        @Override
        public SelectedPrestation[] newArray(int size) {
            return new SelectedPrestation[size];
        }
    };
    private int id;
    private String nom;
    private int categorie_prestations_id;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCategorie_prestations_id() {
        return this.categorie_prestations_id;
    }

    public void setCategorie_prestations_id(int categorie_prestations_id) {
        this.categorie_prestations_id = categorie_prestations_id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
        dest.writeInt(this.categorie_prestations_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
