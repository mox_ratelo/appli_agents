package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Phael on 17/12/2017.
 */

public class Prestation implements Parcelable {

    private int id;
    private int categorie_prestations_id;
    private String nom;

    private Categorie categorie_prestation;
    private boolean isSelected;
    private boolean isInList;

    public Prestation(int id, String name, boolean isSelected) {
        this.id = id;
        this.nom = name;
        this.isSelected = isSelected;
        this.isInList = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return nom;
    }

    public void setName(String name) {
        this.nom = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getCategorie_prestations_id() {
        return categorie_prestations_id;
    }

    public void setCategorie_prestations_id(int categorie_prestations_id) {
        this.categorie_prestations_id = categorie_prestations_id;
    }

    public Categorie getCategorie() {
        return categorie_prestation;
    }

    public void setCategorie(Categorie categorie_prestation) {
        this.categorie_prestation = categorie_prestation;
    }

    public boolean isInList() {
        return isInList;
    }

    public void setInList(boolean inList) {
        isInList = inList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.categorie_prestations_id);
        dest.writeString(this.nom);
        dest.writeParcelable(this.categorie_prestation, flags);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isInList ? (byte) 1 : (byte) 0);
    }

    protected Prestation(Parcel in) {
        this.id = in.readInt();
        this.categorie_prestations_id = in.readInt();
        this.nom = in.readString();
        this.categorie_prestation = in.readParcelable(Categorie.class.getClassLoader());
        this.isSelected = in.readByte() != 0;
        this.isInList = in.readByte() != 0;
    }

    public static final Creator<Prestation> CREATOR = new Creator<Prestation>() {
        @Override
        public Prestation createFromParcel(Parcel source) {
            return new Prestation(source);
        }

        @Override
        public Prestation[] newArray(int size) {
            return new Prestation[size];
        }
    };
}
