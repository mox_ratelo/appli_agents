package app.briconett.models;

import java.util.ArrayList;

/**
 * Created by RATELOSON Andrianiaina Miora (MOX) on 27/12/2017.
 */

public class CategorieViewModel {

    private int id;
    private String nom;
    private int id_prestation;
    private ArrayList<Prestation> prestations;


    public CategorieViewModel(int _id,int _idPresetation,String _nom){
        this.id = _id;
        this.nom = _nom;
        this.id_prestation = _idPresetation;
    }

    public int getId_prestation() {
        return id_prestation;
    }

    public void setId_prestation(int id_prestation) {
        this.id_prestation = id_prestation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public ArrayList<Prestation> getPrestations() {
        return prestations;
    }

    public void setPrestations(ArrayList<Prestation> prestations) {
        this.prestations = prestations;
    }

    @Override
    public String toString() {
        return nom;
    }

}
