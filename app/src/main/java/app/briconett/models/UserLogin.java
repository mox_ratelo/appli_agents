package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Phael on 22/12/2017.
 */

public class UserLogin implements Parcelable {
    private String action;
    private boolean connected;
    private User user;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public User getUser() {
        return user;
    }



    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.action);
        dest.writeByte(this.connected ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.user, flags);
    }

    public UserLogin() {
    }

    protected UserLogin(Parcel in) {
        this.action = in.readString();
        this.connected = in.readByte() != 0;
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<UserLogin> CREATOR = new Creator<UserLogin>() {
        @Override
        public UserLogin createFromParcel(Parcel source) {
            return new UserLogin(source);
        }

        @Override
        public UserLogin[] newArray(int size) {
            return new UserLogin[size];
        }
    };
}
