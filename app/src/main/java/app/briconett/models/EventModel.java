package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Phael on 22/12/2017.
 */

public class EventModel implements Parcelable {

    private  int id;
    private String date;
    private int type;
    private String commentaire;
    private String titre;
    private boolean iscorrective;
    private String description_corrective;
    private int clients_id;
    private int users_id;
    private String signature_client;
    private ArrayList<String> urlImage;
    private long timestampDuration;
    private String dureeeffectuee;
    private String heure_debut;
    private String heure_fin;
    private String dureeprevue;
    private ArrayList<SignEvent> image_signature;

    public String getDureeprevue() {
        return dureeprevue;
    }

    public void setDureeprevue(String dureeprevue) {
        this.dureeprevue = dureeprevue;
    }

    public String getHeure_debut() {
        return heure_debut;
    }

    public void setHeure_debut(String heure_debut) {
        this.heure_debut = heure_debut;
    }

    public String getHeure_fin() {
        return heure_fin;
    }

    public void setHeure_fin(String heure_fin) {
        this.heure_fin = heure_fin;
    }

    private ArrayList<ImageEvent> image_evenements;
    private ArrayList<SelectedPrestation> prestations;

    private String img_signature_file;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDureeeffectuee() {
        return dureeeffectuee;
    }

    public void setDureeeffectuee(String d) {
        this.dureeeffectuee = d;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public boolean isIscorrective() {
        return iscorrective;
    }

    public void setIscorrective(boolean iscorrective) {
        this.iscorrective = iscorrective;
    }

    public String getDescription_corrective() {
        return description_corrective;
    }

    public void setDescription_corrective(String description_corrective) {
        this.description_corrective = description_corrective;
    }

    public ArrayList<SignEvent> getImage_signature() {
        return image_signature;
    }

    public void setImage_signature(ArrayList<SignEvent> image_signature) {
        this.image_signature = image_signature;
    }

    public ArrayList<ImageEvent> getImage_evenements() {
        return image_evenements;
    }

    public void setImage_evenements(ArrayList<ImageEvent> image_evenements) {
        this.image_evenements = image_evenements;
    }

    public int getClients_id() {
        return clients_id;
    }

    public void setClients_id(int clients_id) {
        this.clients_id = clients_id;
    }

    public int getUsers_id() {
        return users_id;
    }

    public void setUsers_id(int users_id) {
        this.users_id = users_id;
    }

    public String getSignature_client() {
        return signature_client;
    }

    public void setSignature_client(String signature_client) {
        this.signature_client = signature_client;
    }

    public ArrayList<String> getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(ArrayList<String> urlImage) {
        this.urlImage = urlImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_signature_file() {
        return img_signature_file;
    }

    public void setImg_signature_file(String img_signature_file) {
        this.img_signature_file = img_signature_file;
    }

    public long getTimestampDuration() {
        return timestampDuration;
    }

    public void setTimestampDuration(long timestampDuration) {
        this.timestampDuration = timestampDuration;
    }

    public ArrayList<SelectedPrestation> getPrestations() {
        return prestations;
    }

    public void setPrestations(ArrayList<SelectedPrestation> prestations) {
        this.prestations = prestations;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.date);
        dest.writeInt(this.type);
        dest.writeString(this.commentaire);
        dest.writeString(this.titre);
        dest.writeByte(this.iscorrective ? (byte) 1 : (byte) 0);
        dest.writeString(this.description_corrective);
        dest.writeInt(this.clients_id);
        dest.writeInt(this.users_id);
        dest.writeString(this.signature_client);
        dest.writeStringList(this.urlImage);
        dest.writeLong(this.timestampDuration);
        dest.writeString(this.dureeeffectuee);
        dest.writeString(this.heure_debut);
        dest.writeString(this.heure_fin);
        dest.writeString(this.dureeprevue);
        dest.writeTypedList(this.image_signature);
        dest.writeTypedList(this.image_evenements);
        dest.writeTypedList(this.prestations);
        dest.writeString(this.img_signature_file);
    }

    public EventModel() {
    }

    protected EventModel(Parcel in) {
        this.id = in.readInt();
        this.date = in.readString();
        this.type = in.readInt();
        this.commentaire = in.readString();
        this.titre = in.readString();
        this.iscorrective = in.readByte() != 0;
        this.description_corrective = in.readString();
        this.clients_id = in.readInt();
        this.users_id = in.readInt();
        this.signature_client = in.readString();
        this.urlImage = in.createStringArrayList();
        this.timestampDuration = in.readLong();
        this.dureeeffectuee = in.readString();
        this.heure_debut = in.readString();
        this.heure_fin = in.readString();
        this.dureeprevue = in.readString();
        this.image_signature = in.createTypedArrayList(SignEvent.CREATOR);
        this.image_evenements = in.createTypedArrayList(ImageEvent.CREATOR);
        this.prestations = in.createTypedArrayList(SelectedPrestation.CREATOR);
        this.img_signature_file = in.readString();
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel source) {
            return new EventModel(source);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };
}
