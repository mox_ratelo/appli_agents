package app.briconett.models;

/**
 * Created by Phael on 10/01/2018.
 */

public class DurationItem {
    private int id;
    private String name;
    private String val;
    private int durationValue;
    private boolean isSelected;

    public DurationItem(int id, String name, int durationValue,String val) {
        this.id = id;
        this.name = name;
        this.durationValue = durationValue;
        this.isSelected = false;
        this.val = val;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDurationValue() {
        return durationValue;
    }

    public void setDurationValue(int durationValue) {
        this.durationValue = durationValue;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
