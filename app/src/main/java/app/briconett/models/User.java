package app.briconett.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Phael on 22/12/2017.
 */

public class User implements Parcelable {

    private int id;
    private String nom;
    private String prenom;
    private String code;
    private String img_profil;
    private String email;
    private String login;
    private int role;
    private String created;
    private String modifed;
    private String base_agent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImg_profil() {
        return img_profil;
    }

    public void setImg_profil(String img_profil) {
        this.img_profil = img_profil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModifed() {
        return modifed;
    }

    public void setModifed(String modifed) {
        this.modifed = modifed;
    }

    public String getBase_agent() {
        return base_agent;
    }

    public void setBase_agent(String base_agent) {
        this.base_agent = base_agent;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
        dest.writeString(this.prenom);
        dest.writeString(this.code);
        dest.writeString(this.img_profil);
        dest.writeString(this.email);
        dest.writeString(this.login);
        dest.writeInt(this.role);
        dest.writeString(this.created);
        dest.writeString(this.modifed);
        dest.writeString(this.base_agent);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.id = in.readInt();
        this.nom = in.readString();
        this.prenom = in.readString();
        this.code = in.readString();
        this.img_profil = in.readString();
        this.email = in.readString();
        this.login = in.readString();
        this.role = in.readInt();
        this.created = in.readString();
        this.modifed = in.readString();
        this.base_agent = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
