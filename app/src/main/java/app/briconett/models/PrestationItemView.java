package app.briconett.models;

/**
 * Created by Phael on 18/12/2017.
 */

public class PrestationItemView {
    private int id;
    private String name;
    private boolean isTitle;
    private String category;
    private int categoryId;

    public PrestationItemView(String name, boolean isTitle,String category) {
        this.name = name;
        this.isTitle = isTitle;
        this.category = category;
    }

    public PrestationItemView(int id ,String name, boolean isTitle, int categoryId) {
        this.id = id;
        this.name = name;
        this.isTitle = isTitle;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTitle() {
        return isTitle;
    }

    public void setTitle(boolean title) {
        isTitle = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
