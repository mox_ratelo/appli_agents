package app.briconett.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.ProgressBar;

import app.briconett.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Phael on 26/12/2017.
 */

public class ProgressDialog extends Dialog {


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    public ProgressDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        ButterKnife.bind(this);

        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#BA233B"), android.graphics.PorterDuff.Mode.SRC_ATOP);
        this.setCancelable(false);
    }
}