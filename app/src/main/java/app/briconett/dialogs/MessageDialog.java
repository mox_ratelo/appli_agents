package app.briconett.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import app.briconett.R;
import app.briconett.tools.GenericTools;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phael on 10/01/2018.
 */

public class MessageDialog extends Dialog {


    private String title;
    private final String message;

    @BindView(R.id.dialogTitle)
    TextView dialogTitle;

    @BindView(R.id.message)
    TextView dialogMessage;
     @BindView(R.id.containerSingleBtn)
    RelativeLayout containerSingleBtn;

    @BindView(R.id.containerTwoBtn)
    LinearLayout containerTwoBtn;
    boolean isSingle;

    IMessageDialogListener listener;


    public MessageDialog(@NonNull Context context,String title, @NonNull String message,boolean isSingle,IMessageDialogListener listener) {
        super(context);
        this.title = title;
        this.message = message;
        this.isSingle = isSingle;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_message);

        ButterKnife.bind(this);
        this.setCancelable(false);
        if(isSingle)
        {
            containerSingleBtn.setVisibility(View.VISIBLE);
            containerTwoBtn.setVisibility(View.GONE);
        }
        else
        {
            containerSingleBtn.setVisibility(View.GONE);
            containerTwoBtn.setVisibility(View.VISIBLE);
        }
        if(!GenericTools.isNullOrEmpty(title))
        {
            dialogTitle.setText(title);
        }

        if(!GenericTools.isNullOrEmpty(message))
        {
            dialogMessage.setText(message);
        }
    }


    @OnClick(R.id.btn_dismiss) void dismissClicked(){
        if (listener != null) {
            listener.messageDialogClicked();
        }
        this.dismiss();
    }
    @OnClick(R.id.btnCancel) void cancelClicked(){
        this.dismiss();
    }

    @OnClick(R.id.btnOk) void okClicked(){
        if (listener != null) {
            listener.messageDialogClicked();
        }
        this.dismiss();
    }

    public interface IMessageDialogListener {
        void messageDialogClicked();
    }


}
