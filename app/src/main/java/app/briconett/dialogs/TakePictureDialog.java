package app.briconett.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import app.briconett.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;



public class TakePictureDialog extends Dialog {


    @BindView(R.id.text_message)
    TextView textViewMessage;
   // @BindView(R.id.button_take_picture)
    Button bntTakePicture;
    @BindView(R.id.button_from_gallery) Button btnFromGallery;


    private final String message;
    private ITakePictureDialogListener listener;


    public TakePictureDialog(@NonNull Context context, @NonNull String message, ITakePictureDialogListener listener) {
        super(context);
        this.message = message;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_take_picture);

        ButterKnife.bind(this);

        this.textViewMessage.setText(this.message);

       // this.bntTakePicture.setVisibility(View.VISIBLE);
        this.btnFromGallery.setVisibility(View.VISIBLE);

        this.setCancelable(true);
    }


    @OnClick(R.id.button_take_picture) void takePictureClicked(){
        if (this.listener != null) {
            this.listener.takePictureClicked();
        }

        this.dismiss();
    }

    @OnClick(R.id.button_from_gallery) void fromGalleryClicked(){
        if (this.listener != null) {
            this.listener.fromGalleryClicked();
        }

        this.dismiss();
    }


    public interface ITakePictureDialogListener {
        void takePictureClicked();
        void fromGalleryClicked();
    }

    @Override
    public void setOnCancelListener(@Nullable OnCancelListener listener) {
        super.setOnCancelListener(listener);
    }
}
