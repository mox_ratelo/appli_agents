package app.briconett.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import app.briconett.R;
import app.briconett.activity.MainActivity;
import app.briconett.components.CustomEditText;
import app.briconett.tools.GenericTools;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phael on 18/12/2017.
 */

public class CommentDialog  extends Dialog {


    @BindView(R.id.edit_comment)
    CustomEditText edit_comment;

    private ICommentListener listener;


    public CommentDialog(Context context, ICommentListener listener) {
        super(context);

        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_comment);

        ButterKnife.bind(this);


        this.setCancelable(true);
    }


    @OnClick(R.id.btn_add) void addComment(){
        if (this.listener != null && !GenericTools.isNullOrEmpty(edit_comment.getText().toString())) {
            MainActivity.comments.add(edit_comment.getText().toString());
            this.listener.refreshCommentList();
        }

        this.dismiss();
    }




    public interface ICommentListener {
        void refreshCommentList();
    }

    @Override
    public void setOnCancelListener( OnCancelListener listener) {
        super.setOnCancelListener(listener);
    }
}
