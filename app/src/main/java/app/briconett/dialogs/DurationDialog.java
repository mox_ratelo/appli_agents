package app.briconett.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TimePicker;

import java.util.ArrayList;

import app.briconett.R;
import app.briconett.adapter.DialogDurationAdapter;
import app.briconett.models.DurationItem;
import app.briconett.tools.AgentSingleton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phael on 10/01/2018.
 */

public class DurationDialog extends Dialog  implements  DialogDurationAdapter.IDurationListener{
    public DurationDialog(@NonNull Context context) {
        super(context);
    }
    public ArrayList<DurationItem> durationItems ;
    private DialogDurationAdapter dialogDurationAdapter;

    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.timePickerHour)
    TimePicker timePickerHour;
    DialogDurationListener dialogDurationListener;

    Activity context;
    public DurationDialog(@NonNull Activity context, DialogDurationListener dialogDurationListener) {
        super(context);
        this.context = context;
        this.dialogDurationListener = dialogDurationListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //*getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_duration);
        ButterKnife.bind(this);
        this.setCancelable(false);
        prepareAdapter();
        timePickerHour.setIs24HourView(true);
    }

    private void prepareAdapter() {
        durationItems = new  ArrayList<>();
        dialogDurationAdapter = new DialogDurationAdapter(context, durationItems,this);
    }


    @OnClick(R.id.btn_ok)void onBtnOkClicked()
    {
        int heure = 0;
        int minute = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            heure =  timePickerHour.getHour();
            minute = timePickerHour.getMinute();
        }
        else {
            heure =  timePickerHour.getCurrentHour();
            minute = timePickerHour.getCurrentMinute();
        }
        String duree = "";
        if(heure>0){
            duree = heure+"h "+minute+"min";
            DurationItem durationItem = new DurationItem(1,duree,minute+heure*60,duree);
            durationItems.add(durationItem);
            AgentSingleton.getInstance().heurreEstimer = duree;
            dialogDurationListener.initTimer(durationItem.getDurationValue() * 1000* 60);
            dismiss();
        }
        else{
            duree = minute+"min";
            DurationItem durationItem = new DurationItem(1,duree,minute,duree);
            durationItems.add(durationItem);
            AgentSingleton.getInstance().heurreEstimer = duree;
            dialogDurationListener.initTimer(durationItem.getDurationValue() * 1000* 60);
            dismiss();

        }
    }

    private DurationItem getDurationSelected() {
        for(DurationItem durationItem : durationItems)
        {
            if(durationItem.isSelected())
            {
                return durationItem;
            }
        }
        return null;
    }

    @Override
    public void durationSelected(int position, boolean isChecked) {
    }

    public interface DialogDurationListener {

        void initTimer(long millisecond);
    }
}

