package app.briconett.dialogs;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import app.briconett.R;
import app.briconett.components.CustomEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phael on 24/01/2018.
 */

public class EndInterventionDialog extends Dialog{

    Context context;
    @BindView(R.id.confirmPwd)
    CustomEditText confirmPwd;
    @BindView(R.id.editPwd)
    CustomEditText editPwd;
    IEndInterventionListener listener;

    public EndInterventionDialog(Context context,IEndInterventionListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_end_intervention);

        ButterKnife.bind(this);
        this.setCancelable(false);
    }


    @OnClick(R.id.btnScan) void onBtnScanClicked()
    {
        listener.link1Clicked();
        this.dismiss();
    }


    @OnClick(R.id.btnLink2) void onBtnLink2Clicked()
    {
        this.dismiss();
        listener.link2Clicked();
    }

    @OnClick(R.id.btnLink3) void onBtnLink3Clicked()
    {
        this.dismiss();
        listener.link3Clicked();
    }

    public interface IEndInterventionListener {
        void link1Clicked();
        void link2Clicked();
        void link3Clicked();
    }
}
