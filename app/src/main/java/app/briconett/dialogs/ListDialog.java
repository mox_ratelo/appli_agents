package app.briconett.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;
import app.briconett.R;
import app.briconett.adapter.DialogListAdapter;
import app.briconett.models.PrestationItemView;

import app.briconett.tools.AgentSingleton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phael on 17/12/2017.
 */

public class ListDialog extends Dialog{

    IListDialogListener listDialogListener;
    @BindView(R.id.dialog_list_prestation_type)
    ListView listView;
    @BindView(R.id.btn_cancel) Button btnCancel;
    @BindView(R.id.btn_add) Button btnAdd;
    DialogListAdapter adapter;
    ArrayList<PrestationItemView> items;


    Activity context;
    public ListDialog(@NonNull Context context) {
        super(context);
    }
    public ListDialog(@NonNull Activity context, IListDialogListener listDialogListener) {
        super(context);
        this.context = context;
        this.listDialogListener = listDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //*getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_list);
        ButterKnife.bind(this);
        this.setCancelable(false);
        prepareAdapter();
    }

    private void prepareAdapter() {

        adapter = new DialogListAdapter(context,AgentSingleton.getInstance().prestationArrayList, new DialogListAdapter.ICheckBoxListener() {
            @Override
            public void changeState(int position, boolean state) {
                if(state)
                {
                }
            }

            @Override
            public void refreshList() {
                adapter.notifyDataSetChanged();
            }
        });

        listView.setAdapter(adapter);
    }

    @OnClick(R.id.btn_add)void onBtnAddClicked()
    {
       listDialogListener.addPrestation();

        this.dismiss();
    }

    @OnClick(R.id.btn_cancel)void onCancel()
    {
       listDialogListener.cancel();
       this.dismiss();
    }



    public interface IListDialogListener {
        void cancel();
        void addPrestation();

        void refreshList();
    }



}
