package app.briconett.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;

import app.briconett.R;
import app.briconett.components.CustomEditText;
import app.briconett.tools.GenericTools;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phael on 10/01/2018.
 */

public class UpadePwdDialog extends Dialog {

    Context context;
    @BindView(R.id.confirmPwd)
    CustomEditText confirmPwd;
    @BindView(R.id.editPwd)
    CustomEditText editPwd;
    IUpdatePasswordListener listener;

    public UpadePwdDialog(@NonNull Context context,IUpdatePasswordListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_edit_pwd);

        ButterKnife.bind(this);
        this.setCancelable(true);
    }


        @OnClick(R.id.btn_dismiss) void takePictureClicked(){
        //this.dismiss();
            String password = editPwd.getText().toString();

            if(GenericTools.isNullOrEmpty(password))
            {
                editPwd.setError("Saisissez le nouveau mot de passe");
                return;
            }
            if(password.length()<8)
            {

               // editPwd.setError("Le mot de passe doit contenir au moins 8 caractères");
                //return;

            }
            String cofirmPassword  = confirmPwd.getText().toString();
            if(GenericTools.isNullOrEmpty(cofirmPassword))
            {
                confirmPwd.setError("Veuillez confirmé votre mot de passe");
                return;
            }
            if(!password.equals(cofirmPassword))
            {
                confirmPwd.setError("Veuillez confirmé votre mot de passe");
                return;
            }
            this.dismiss();
            listener.updatePassword(password);
        }


    public interface IUpdatePasswordListener {
        void updatePassword(String password);
    }

}
