package app.briconett.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import app.briconett.activity.MainActivity;

public class BaseFragment extends Fragment {
    public MainActivity mainActivity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = ((MainActivity)getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
