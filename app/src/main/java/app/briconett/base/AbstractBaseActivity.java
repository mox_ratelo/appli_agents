package app.briconett.base;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import app.briconett.dialogs.ProgressDialog;
import app.briconett.interfaces.IPermissionsGranted;
import app.briconett.R;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Phael on 14/12/2017.
 */

public abstract class AbstractBaseActivity extends AppCompatActivity {

    protected static final String TAG = AbstractBaseActivity.class.getName();

    protected ProgressDialog progressDialog;
    private IPermissionsGranted iPermissionsGranted;

        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(getActivityView());

            ButterKnife.bind(this);
        }


    protected abstract int getActivityView();



    /**
     * Launch an activity
     * @param destinationActivity
     * @param params Parameters
     * @param needToFinish set true if need to finish the activity recipient.
     */
    public void launchActivity(Class destinationActivity, @Nullable Bundle params, boolean needToFinish){
        Intent intent = new Intent(this, destinationActivity);
        if (params != null){
            intent.putExtras(params);
        }
        startActivity(intent);
        if (needToFinish){
            finish();
        }
    }

    public boolean methodRequiresSignaturePermission(IPermissionsGranted _iPermissionsGranted){

        iPermissionsGranted = _iPermissionsGranted;

        String[] perms = {

                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };


        if (EasyPermissions.hasPermissions(this, perms)) {

            return true;

        }else {

            EasyPermissions.requestPermissions(this, getString(R.string.app_name) + getString(R.string.string_need_permissions),
                    1, perms);

        }

        return false;
    }


    public void showProgressDialog() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                progressDialog = new ProgressDialog(AbstractBaseActivity.this);
                progressDialog.show();
            }
        });


    }


    public void dismissProgressDialog() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });



    }

}
