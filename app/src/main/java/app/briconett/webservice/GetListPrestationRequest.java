package app.briconett.webservice;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.briconett.models.EventModel;
import app.briconett.models.Prestation;
import app.briconett.tools.Constant;
import app.briconett.tools.GenericTools;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Phael on 23/12/2017.
 */

public class GetListPrestationRequest {

    IGetListPrestationRequestListener listener;
    Activity context;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static int TIME_OUT_VALUE = 60;
    private static int CONNECTION_TIME_OUT = 10;
    private final String TAG = this.getClass().getCanonicalName();
    public GetListPrestationRequest(Activity context, IGetListPrestationRequestListener listener) {
        this.listener = listener;
        this.context = context;
    }

    public void execute() {

        final boolean isDeviceOnLine = GenericTools.isDeviceOnline(this.context);
       // if (!isDeviceOnLine)
        //{
          //  listener.onGetListPrestationRequestError("device offline");
            //return;
        //}
        final OkHttpClient client = new OkHttpClient.Builder().connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .build();

        final Request request = new Request.Builder()
                .url(Constant.BASE_URL + "prestations/listprestation")
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "Failed to execute " + request, e);
                listener.onGetListPrestationRequestError("request failed");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String responseBody = response.body().string();
                Log.d(TAG, "Body :" + responseBody);

                if (!response.isSuccessful()) {
                    listener.onGetListPrestationRequestError("error");
                    return;

                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            Gson gson = new Gson();
                            try
                            {
                                JSONArray itemJsonArray =  new JSONArray(responseBody);
                                ArrayList<Prestation> prestations = new ArrayList<Prestation>();
                                if(itemJsonArray!=null && itemJsonArray.length()>0) {
                                    for(int i = 0;i<itemJsonArray.length();i++){
                                        Prestation prestation = gson.fromJson(itemJsonArray.getJSONObject(i).toString(),Prestation.class);
                                        prestations.add(prestation);
                                    }
                                }
                                listener.onGetListPrestationRequestSucces(prestations);
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Error when parsing : " + e.getLocalizedMessage());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                });

            }

        });
    }

    public interface IGetListPrestationRequestListener
    {
        void onGetListPrestationRequestSucces(ArrayList<Prestation> eventModels);
        void onGetListPrestationRequestError(String message);
    }
}
