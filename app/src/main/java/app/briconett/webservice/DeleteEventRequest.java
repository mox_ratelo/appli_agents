package app.briconett.webservice;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import app.briconett.tools.Constant;
import app.briconett.tools.GenericTools;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Phael on 14/01/2018.
 */

public class DeleteEventRequest {
    //https://­client-briconett.fr/­evenements/­deleteevenement/517

    IDeleteEventRequestListener listener;
    Activity context;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static int TIME_OUT_VALUE = 60;
    private static int CONNECTION_TIME_OUT = 10;
    private final String TAG = this.getClass().getCanonicalName();

    public DeleteEventRequest(Activity context, IDeleteEventRequestListener listener) {
        this.listener = listener;
        this.context = context;
    }

    public void execute(String id) {

        final boolean isDeviceOnLine = GenericTools.isDeviceOnline(this.context);
        if (!isDeviceOnLine)
        {
            listener.onDeleteEventRequestError("device offline");
            return;
        }
        final OkHttpClient client = new OkHttpClient.Builder().connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .build();

        final Request request = new Request.Builder()
                .url(Constant.BASE_URL + "evenements/deleteevenement"+id)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "Failed to execute " + request, e);
                listener.onDeleteEventRequestError("request failed");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String responseBody = response.body().string();
                Log.d(TAG, "Body :" + responseBody);

                if (!response.isSuccessful()) {
                    listener.onDeleteEventRequestError("error");
                    return;

                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            Gson gson = new Gson();
                            try
                            {

                                listener.onDeleteEventRequestSucces();
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Error when parsing : " + e.getLocalizedMessage());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                });

            }

        });
    }

    public interface IDeleteEventRequestListener
    {
        void onDeleteEventRequestSucces();
        void onDeleteEventRequestError(String message);
    }
}
