package app.briconett.webservice;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import app.briconett.models.EventModel;
import app.briconett.models.UserLogin;
import app.briconett.tools.Constant;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Phael on 23/12/2017.
 */

public class AddEventRequest {


    IAddEventRequestListener listener;
    Activity context;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static int TIME_OUT_VALUE = 60;
    private static int CONNECTION_TIME_OUT = 10;
    private final String TAG = this.getClass().getCanonicalName();
    public AddEventRequest(Activity context, IAddEventRequestListener listener) {
        this.listener = listener;
        this.context = context;
    }

    public void execute(EventModel eventModel){

        /** Création des paramètres */

        HashMap params = new HashMap();

        params.put("date",eventModel.getDate());

        params.put("type",eventModel.getType());
        params.put("commentaire",eventModel.getCommentaire());
        params.put("titre",eventModel.getTitre());
        params.put("iscorrective",eventModel.isIscorrective());
        params.put("description_corrective",eventModel.getDescription_corrective());
        params.put("clients_id",eventModel.getClients_id());
        params.put("users_id",eventModel.getUsers_id());
        params.put("signature_client",eventModel.getSignature_client());
        //params.put("password",password);

        final OkHttpClient client = new OkHttpClient.Builder().connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .build();

        JSONObject jsonParams = new JSONObject(params);
        RequestBody body = RequestBody.create(JSON, String.valueOf(jsonParams));

        final Request request = new Request.Builder()
                .url(Constant.BASE_URL + "evenements/ajoutevenement")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "Failed to execute " + request, e);
                listener.onAddEventRequestError("request failed");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String responseBody = response.body().string();
                Log.d(TAG, "Body :" + responseBody);

                if (!response.isSuccessful()) {
                    listener.onAddEventRequestError("error");
                    return;

                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            Gson gson = new Gson();
                            try
                            {
                                //UserLogin userLogin =  gson.fromJson(responseBody, UserLogin.class);
                               // listener.addEventRequestSucces(userLogin);
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Error when parsing : " + e.getLocalizedMessage());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                });

            }

        });

    }

    public interface IAddEventRequestListener
    {
        void addEventRequestSucces();
        void onAddEventRequestError(String message);
    }
}
