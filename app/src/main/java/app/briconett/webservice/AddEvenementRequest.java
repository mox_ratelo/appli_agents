package app.briconett.webservice;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import app.briconett.models.User;
import app.briconett.tools.Constant;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Phael on 22/12/2017.
 */

public class AddEvenementRequest {
    IAddEvenementRequestListener listener;
    Activity context;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static int TIME_OUT_VALUE = 60;
    private static int CONNECTION_TIME_OUT = 10;
    private final String TAG = this.getClass().getCanonicalName();
    public AddEvenementRequest(Activity context, IAddEvenementRequestListener listener) {
        this.listener = listener;
        this.context = context;
    }

    public void execute(boolean is_sur_place,String _date, String _commentaire, String _titre, boolean isCorrective, String _descAC,
                        int idClient, int idUser, File img1, File img2,File img3,File img4, File imgSignature, ArrayList<String> prestationsId,
                        String _nomClient, String _dureeprevue , String _dureeeffectuee , String _dureediff, String _debut , String _fin){

        /** Création des paramètres */

        final OkHttpClient client = new OkHttpClient.Builder().connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .build();

        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        multipartBuilder.setType(MultipartBody.FORM);
        if (img1 != null){
            multipartBuilder.addFormDataPart("img[]", "image_1.jpg",RequestBody.create(MediaType.parse("image/jpeg"),img1));
        }
        if (img2 != null){
            multipartBuilder.addFormDataPart("img[]",  "image_2.jpg",RequestBody.create(MediaType.parse("image/jpeg"),img2));
        }
        if (img3 != null){
            multipartBuilder.addFormDataPart("img[]",  "image_3.jpg",RequestBody.create(MediaType.parse("image/jpeg"),img3));
        }
        if (img4 != null){
            multipartBuilder.addFormDataPart("img[]",  "image_4.jpg",RequestBody.create(MediaType.parse("image/jpeg"),img4));
        }
        if (imgSignature != null){
            multipartBuilder.addFormDataPart("img_signature[]",  "image_signature.jpg",RequestBody.create(MediaType.parse("image/jpeg"),imgSignature));
        }
        multipartBuilder.addFormDataPart("date", _date);
        multipartBuilder.addFormDataPart("type", "3");
        multipartBuilder.addFormDataPart("commentaire", _commentaire);
        multipartBuilder.addFormDataPart("titre", _titre);
        multipartBuilder.addFormDataPart("iscorrective", (isCorrective) ? "1" : "0");
        multipartBuilder.addFormDataPart("description_corrective", _descAC);
        multipartBuilder.addFormDataPart("clients_id", String.valueOf(idClient));
        multipartBuilder.addFormDataPart("users_id", String.valueOf(idUser));
        multipartBuilder.addFormDataPart("signature_client", _nomClient);
        multipartBuilder.addFormDataPart("dureeprevue", _dureeprevue);
        multipartBuilder.addFormDataPart("dureeeffectuee", _dureeeffectuee);
        multipartBuilder.addFormDataPart("dureediff", _dureediff);
        multipartBuilder.addFormDataPart("heure_debut", _debut);
        multipartBuilder.addFormDataPart("heure_fin", _fin);
        multipartBuilder.addFormDataPart("is_sur_place", (is_sur_place) ? "1" : "0");

       /* for (String str : prestationsId){
            multipartBuilder.addFormDataPart("prestations[_ids][]", str);
        }*/

        RequestBody requestBody = multipartBuilder.build();

        final Request request = new Request.Builder()
                .url(Constant.BASE_URL + "evenements/ajoutevenement")
                .post(requestBody)
                .build();


        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "Failed to execute " + request, e);
                listener.addEventError("request failed");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String responseBody = response.body().string();
                Log.d(TAG, "Body :" + responseBody);

                if (listener != null) {
                    listener.addEventSucces();
                }

            }

        });

    }

    public interface IAddEvenementRequestListener
    {
        void addEventSucces();
        void addEventError(String message);
    }
}

