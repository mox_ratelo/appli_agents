package app.briconett.webservice;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.briconett.models.Client;
import app.briconett.models.EventModel;
import app.briconett.tools.Constant;
import app.briconett.tools.GenericTools;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Phael on 22/12/2017.
 */

public class AccessClientRequest {

    IAccessClientRequestListener listener;
    Activity context;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static int TIME_OUT_VALUE = 60;
    private static int CONNECTION_TIME_OUT = 10;
    private final String TAG = this.getClass().getCanonicalName();
    public AccessClientRequest(Activity context, IAccessClientRequestListener listener) {
        this.listener = listener;
        this.context = context;
    }

    public void execute(String codeApp) {

        final boolean isDeviceOnLine = GenericTools.isDeviceOnline(this.context);
        if (!isDeviceOnLine)
        {
           listener.onAccessClientRequestError("device offline");
           return;
        }
        final OkHttpClient client = new OkHttpClient.Builder().connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_VALUE, TimeUnit.SECONDS)
                .build();

        //Content-Type →text/html; charset=UTF-8

        final Request request = new Request.Builder()
                    .url(Constant.BASE_URL + "clients/identificationclient/" + codeApp)
                    .get()
                    .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, final IOException e) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "Failed to execute " + request, e);
                        listener.onAccessClientRequestError("request failed");
                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String responseBody = response.body().string();
                Log.d(TAG, "Body :" + responseBody);

                if (!response.isSuccessful()) {
                    listener.onAccessClientRequestError("error");
                    return;

                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            Gson gson = new Gson();
                            try
                            {
                                ArrayList<Client> eventModels  = gson.fromJson(responseBody, new TypeToken<List<Client>>(){}.getType());

                                if (eventModels!= null && eventModels.size() > 0){
                                    Client client1 = eventModels.get(0);
                                    listener.onAccessClientRequestSucces(client1);
                                }else {
                                    listener.onAccessClientRequestSucces(null);
                                }


                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Error when parsing : " + e.getLocalizedMessage());
                                listener.onAccessClientRequestError("error parse");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                });

            }

        });
    }

    public interface IAccessClientRequestListener
    {
        void onAccessClientRequestSucces(Client client);
        void onAccessClientRequestError(String message);
    }
}
