package app.briconett;

import android.app.Application;
import android.graphics.Bitmap;
import android.os.StrictMode;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import app.briconett.models.Client;
import app.briconett.tools.GPSTrackerService;

/**
 * Created by Phael on 16/12/2017.
 */

public class AgentApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //Allowing Strict mode policy for Nougat support
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        initImageLoader();
        GPSTrackerService gpsTrackerService = new GPSTrackerService(this);

    }

    private void initImageLoader() {
        DisplayImageOptions defaultDisplayOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .displayer(new RoundedBitmapDisplayer(5))
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration configImageLoader = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .memoryCacheSize(2 * 4096 * 4096)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .defaultDisplayImageOptions(defaultDisplayOptions)
                .build();
        ImageLoader.getInstance().init(configImageLoader);
    }

}
