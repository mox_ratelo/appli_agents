package app.briconett.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import app.briconett.activity.MainActivity;
import app.briconett.R;
import app.briconett.base.BaseFragment;
import app.briconett.tools.GenericTools;

import static app.briconett.activity.MainActivity.prest;

public class PrestationFragment extends BaseFragment {
    Button buttonvalidation;
    TextView name;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static PrestationFragment newInstance() {
        return new PrestationFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prestation, container, false);
        buttonvalidation  = (Button) view.findViewById(R.id.addpres);
        name = (TextView) view.findViewById(R.id.nameprestation);
        buttonvalidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prest.add(name.getText().toString());
                int t = prest.size();
               if(!GenericTools.isNullOrEmpty(name.getText().toString())) mainActivity.pushFragment(InterventionFragment.newInstance(prest));
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
       // ((MainActivity) getActivity()).currentFragment = this;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


}