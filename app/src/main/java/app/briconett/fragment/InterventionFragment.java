package app.briconett.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.OnMapReadyCallback;
import com.androidmapsextensions.SupportMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import app.briconett.adapter.CommentAdapter;
import app.briconett.adapter.PrestationAdapter;
import app.briconett.base.BaseFragment;
import app.briconett.dialogs.DurationDialog;
import app.briconett.dialogs.MessageDialog;
import app.briconett.interfaces.IImageSelected;
import app.briconett.interfaces.IPermissionsGranted;
import app.briconett.BuildConfig;
import app.briconett.activity.MainActivity;
import app.briconett.R;
import app.briconett.dialogs.CommentDialog;
import app.briconett.dialogs.ListDialog;
import app.briconett.dialogs.TakePictureDialog;
import app.briconett.models.CategorieViewModel;
import app.briconett.models.Client;
import app.briconett.models.EventModel;
import app.briconett.models.ImageEvent;
import app.briconett.models.Intervention;
import app.briconett.models.Prestation;
import app.briconett.models.PrestationItemView;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.Constant;
import app.briconett.tools.GPSTrackerService;
import app.briconett.tools.GenericTools;
import app.briconett.tools.ResizeImageAsyncTask;
import app.briconett.webservice.AddEvenementRequest;
import app.briconett.webservice.GetListPrestationRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static app.briconett.activity.MainActivity.depoussierage;
import static app.briconett.activity.MainActivity.netoyage;
import static app.briconett.activity.MainActivity.vitreries;
import static app.briconett.tools.Constant.REQUEST_TAKE_PHOTO;


public class InterventionFragment  extends BaseFragment implements OnMapReadyCallback,
        TakePictureDialog.ITakePictureDialogListener,
        ResizeImageAsyncTask.IResizeImageListener,ListDialog.IListDialogListener,IPermissionsGranted,IImageSelected {
    @BindView(R.id.addprestation)Button addprestation;

    @BindView(R.id.btn_add_signature)Button addSignature;

    @BindView(R.id.imageInserted_1)ImageView imageInserted1;
    @BindView(R.id.imageInserted_2)ImageView imageInserted2;
    @BindView(R.id.imageInserted_3)ImageView imageInserted3;
    @BindView(R.id.imageInserted_4)ImageView imageInserted4;
    @BindView(R.id.listprestation)ListView listprestation;
    @BindView(R.id.liste_comment)ListView liste_comment;
    @BindView(R.id.datesignature)TextView datesignature;
    @BindView(R.id.text_pictures_count)TextView text_pictures_count;
    @BindView(R.id.containerPhotos)LinearLayout containerPhotos;

    @BindView(R.id.layouDescActionC)LinearLayout layouDescActionC;
    @BindView(R.id.txtDescriptionAC)EditText txtDescriptionAC;
    @BindView(R.id.checkBoxAction)CheckBox checkBoxAction;
    @BindView(R.id.imageSignature)ImageView imageSignature;

    @BindView(R.id.btn_end)Button btn_end;

    @BindView(R.id.txtCommentair)EditText txtCommentaire;
    @BindView(R.id.text_no_comment)TextView text_no_comment;

    @BindView(R.id.relativeLayout)LinearLayout relativeLayout;

    @BindView(R.id.btn_add_comment)Button btn_add_comment;
    @BindView(R.id.dureeestime)TextView duree;
    @BindView(R.id.textNomInterne)TextView textNomInterne;
    @BindView(R.id.textAdresse)TextView textAdresse;
    @BindView(R.id.textCpVille)TextView textCpVille;

    @BindView(R.id.layoutImgSignature)LinearLayout layoutSign;
     @BindView(R.id.layoutPic1)RelativeLayout layoutPic1;
    @BindView(R.id.layoutPic2)RelativeLayout layoutPic2;
    @BindView(R.id.layoutPic3)RelativeLayout layoutPic3;
    @BindView(R.id.layoutPic4)RelativeLayout layoutPic4;
    @BindView(R.id.btnDeletPic1)ImageView btnDeletPic1;
    @BindView(R.id.btnDeletPic2)ImageView btnDeletPic2;
    @BindView(R.id.btnDeletPic3)ImageView btnDeletPic3;
    @BindView(R.id.btnDeletPic4)ImageView btnDeletPic4;

   // @BindView(R.id.date)TextView date;
    private boolean isTakePicture;
    private String filePathTemp = null;
    private String fileNameTemp = null;
    private static String prestation;


    static ArrayList<String> prestations;
    PrestationAdapter adapter;
    CommentAdapter commentAdapter;
    static String datedebut;

    SupportMapFragment mapFragment;
    GoogleMap map;
    LatLng myPosition;


    String mCurrentPhotoPath;
    private Uri imageFileUri;
    private int currentImageId;

    static File image1,image2,image3,image4,imageSign;

    static ArrayList<String> listPrestationId;
    static CheckBox ch = null;
    static EditText txtDesc = null;
    static String txtCom = null;
    static boolean hasData = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static InterventionFragment newInstance() {
        hasData = false;
        if(AgentSingleton.getInstance().debut==null) {
            datedebut = new SimpleDateFormat("dd/MM/yy - HH:mm").format(new Date());
            AgentSingleton.getInstance().debut = new SimpleDateFormat("HH:mm").format(new Date());

        }
        return new InterventionFragment();
    }
    public static InterventionFragment newInstance(ArrayList<String> prest) {
        hasData = false;
        if(AgentSingleton.getInstance().debut==null) {
            datedebut = new SimpleDateFormat("dd/MM/yy - HH:mm").format(new Date());
            AgentSingleton.getInstance().debut = new SimpleDateFormat("HH:mm").format(new Date());
            ;
            prestations = prest;
        }
        return new InterventionFragment();
    }
    public static InterventionFragment newInstance(Date d) {
        hasData = false;
        if(AgentSingleton.getInstance().debut==null) {
            datedebut = new SimpleDateFormat("dd/MM/yy - HH:mm").format(d);
            AgentSingleton.getInstance().debut = new SimpleDateFormat("HH:mm").format(new Date());
        }
        return new InterventionFragment();
    }
    public static InterventionFragment newInstance(CheckBox _checkBoxAction,EditText _txtDescriptionAC, File _image1,File _image2,File _image3,File _image4,File _imageSign,ArrayList<String> _listPrestationId )
    {
        hasData = true;
        ch = _checkBoxAction;
        txtDesc = _txtDescriptionAC;
        image1 = _image1;
        image2 = _image2;
        image3 = _image3;
        image4 = _image4;
        imageSign = _imageSign;
        listPrestationId = _listPrestationId;
        if(AgentSingleton.getInstance().interv.getTitre()!=null)  txtCom = AgentSingleton.getInstance().interv.getTitre();
        return new InterventionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intervention, container, false);
        ButterKnife.bind(this, view);
       // duree.setText("Durée estimée : "+AgentSingleton.getInstance().heurreEstimer);
      //  duree.setText("Durée estimée : ");
        if(AgentSingleton.getInstance().beginDate!=null) datesignature.setText(new SimpleDateFormat("HH:mm").format(AgentSingleton.getInstance().beginDate));
       else datesignature.setText(new SimpleDateFormat("dd/MM/yy - HH:mm").format(new Date()));
        textNomInterne.setText(AgentSingleton.getInstance().client.getNom());
        textAdresse.setText(AgentSingleton.getInstance().client.getAdresse());
        textCpVille.setText(AgentSingleton.getInstance().client.getCp() + " " + AgentSingleton.getInstance().client.getVille());

        //date.setText(datedebut);
        createMapFragment();
        if(txtCom!=null) txtCommentaire.setHint(txtCom);
        if(txtCom!=null) txtCommentaire.setText(txtCom);
        if(txtDesc!=null) txtDescriptionAC.setText(txtDesc.getText());
        if(ch!=null) checkBoxAction = ch;
        if(AgentSingleton.getInstance().heurreEstimer!=null) duree.setText(AgentSingleton.getInstance().heurreEstimer);
        if(hasData){
            initViewData();
        }
        mainActivity.setiImageSelected(this);
        return view;
    }
    public void initViewData(){
        boolean hasImage = false;
        int nbImage =0;
        if(image1!=null && image1.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(image1.getAbsolutePath());
            imageInserted1.setImageBitmap(bitmap);
            layoutPic1.setVisibility(View.VISIBLE);
            hasImage = true;
            nbImage ++;
        }
        if(image2!=null ){
            Bitmap bitmap = BitmapFactory.decodeFile(image2.getAbsolutePath());
            imageInserted2.setImageBitmap(bitmap);
            layoutPic2.setVisibility(View.VISIBLE);
            hasImage = true;
            nbImage ++;
        }
        if(image3!=null ){
            Bitmap bitmap = BitmapFactory.decodeFile(image3.getAbsolutePath());
            imageInserted3.setImageBitmap(bitmap);
            layoutPic3.setVisibility(View.VISIBLE);
            hasImage = true;
            nbImage ++;
        }
        if(image4!=null ){
            Bitmap bitmap = BitmapFactory.decodeFile(image4.getAbsolutePath());
            imageInserted4.setImageBitmap(bitmap);
            layoutPic4.setVisibility(View.VISIBLE);
            hasImage = true;
            nbImage ++;
        }
        if(hasImage)
        {
            text_pictures_count.setText(nbImage +" photo(s)");
            containerPhotos.setVisibility(View.VISIBLE);
        }
        else
        {
            text_pictures_count.setText("Aucune photo");
            containerPhotos.setVisibility(View.GONE);
        }
        if(imageSign != null)
        {
            Bitmap bitmap = BitmapFactory.decodeFile(imageSign.getAbsolutePath());
            imageSignature.setImageBitmap(bitmap);
            imageSignature.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            layoutSign.setVisibility(View.VISIBLE);

        }
        if(!GenericTools.isNullOrEmpty(txtCom))
        {
            text_no_comment.setVisibility(View.GONE);
            txtCommentaire.setVisibility(View.VISIBLE);
            txtCommentaire.setText(txtCom);

        }
    }
    @Override
    public void onResume() {
        super.onResume();
        //((MainActivity)getActivity()).currentFragment = this;
        GenericTools.hiddenKeyboard(getActivity());
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListPrestationSelected();
        initCommentList();

        checkBoxAction.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(checkBoxAction.isChecked()){
                    layouDescActionC.setVisibility(View.VISIBLE);
                }else{
                    layouDescActionC.setVisibility(View.GONE);
                }
            }
        });
        ((View)checkBoxAction.getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxAction.performClick();
            }
        });

        if(AgentSingleton.getInstance().interv==null) {
            DurationDialog durationDialog = new DurationDialog(getActivity(), new DurationDialog.DialogDurationListener() {
                @Override
                public void initTimer(long millisecond) {
                    ((MainActivity) getActivity()).initCountDownTimer(millisecond, 1000);
                    AgentSingleton.getInstance().beginDate = new Date();
                    AgentSingleton.getInstance().debut = new SimpleDateFormat("HH:mm").format(new Date());

                    if (AgentSingleton.getInstance().heurreEstimer != null) {
                        duree.setText(" " + AgentSingleton.getInstance().heurreEstimer);
                        AgentSingleton.getInstance().interv = new EventModel();
                    }
                }
            });
            durationDialog.show();
        }

    }
    private void addlist(String name){
        prestations.add(name);

    }
    private void getListPrestationSelected() {

        ArrayList<Prestation> prestationItemViews = new ArrayList<>();
        for(Prestation prestation : AgentSingleton.getInstance().prestationArrayList)
        {
            if (prestation.isSelected()) {
                prestation.setInList(true);
                prestationItemViews.add(prestation);
            }
        }

        adapter = new PrestationAdapter(prestationItemViews, mainActivity, new PrestationAdapter.IPrestationListener() {
            @Override
            public void deleteItem(int id) {

                GenericTools.setListViewHeightBasedOnChildren(listprestation,getActivity());
            }
        });
        listprestation.setAdapter(adapter);
        if(prestationItemViews.size()!=0)listprestation.setVisibility(View.VISIBLE);
        else listprestation.setVisibility(View.GONE);
        GenericTools.setListViewHeightBasedOnChildren(listprestation,getActivity());

    }


    @OnClick(R.id.btnDeletPic1)void  deleteImage1Clicked()
    {

        layoutPic1.setVisibility(View.GONE);
        image1 = null;
        imageInserted1.setImageDrawable(null);

        getCountImages();
    }

    @OnClick(R.id.btnDeletPic2)void  deleteImage2Clicked()
    {

        layoutPic2.setVisibility(View.GONE);
        image2 = null;
        imageInserted2.setImageDrawable(null);

        getCountImages();
    }
    @OnClick(R.id.btnDeletPic3)void  deleteImage3Clicked()
    {

        layoutPic3.setVisibility(View.GONE);
        image3 = null;
        imageInserted3.setImageDrawable(null);

        getCountImages();
    }
    @OnClick(R.id.btnDeletPic4)void  deleteImage4Clicked()
    {

        layoutPic4.setVisibility(View.GONE);
        image4 = null;
        imageInserted4.setImageDrawable(null);

        getCountImages();
    }
    @OnClick(R.id.btnDeleteSign)void  deleteSign()
    {
        layoutSign.setVisibility(View.GONE);
        imageSign = null;
    }

    @OnClick(R.id.btn_add_comment)void  onAddCommentClicked()
    {

        text_no_comment.setVisibility(View.GONE);
        txtCommentaire.setVisibility(View.VISIBLE);

        txtCommentaire.requestFocus();

        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                txtCommentaire.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);

    }

    @OnClick(R.id.relativeLayout)void  onParentViewClicked()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    @OnClick(R.id.btn_end)void  onAddInterventionClicked()
    {

        AgentSingleton.getInstance().interv.setTitre(txtCommentaire.getText().toString());
        AgentSingleton.getInstance().isEndScanQRC = true;
        mainActivity.pushFragment(EndInterventionFragment.newInstance(checkBoxAction , txtDescriptionAC , image1,image2,image3,image4,imageSign,listPrestationId));
    }


    private void getIdPrestationSelected() {
        listPrestationId = new  ArrayList<>();
        for(Prestation prestation : AgentSingleton.getInstance().prestationArrayList)
        {
            if(prestation.isSelected())
            {
                listPrestationId.add(String.valueOf(prestation.getId()));
            }
        }
    }


    @OnClick(R.id.btn_add_pictures)void  onImageClicked()
    {
        if(GenericTools.imageViewhasImage(imageInserted1) && GenericTools.imageViewhasImage(imageInserted2) && GenericTools.imageViewhasImage(imageInserted3) && GenericTools.imageViewhasImage(imageInserted4)){
            //return;
            MessageDialog dial = new MessageDialog(getContext(), "Notification", "Vous pouvez ajouter 4 photos au maximum", true, new MessageDialog.IMessageDialogListener() {
                    @Override
                    public void messageDialogClicked() {
                      return;
                    }
            });
            dial.show();
        }

        if(!GenericTools.imageViewhasImage(imageInserted1))
        {
            currentImageId = R.id.imageInserted_1;
            showDialogTakePicture();
        }
        else if(!GenericTools.imageViewhasImage(imageInserted2))
        {
            currentImageId = R.id.imageInserted_2;
            showDialogTakePicture();
        }
        else if(!GenericTools.imageViewhasImage(imageInserted3))
        {
            currentImageId = R.id.imageInserted_3;
            showDialogTakePicture();
        }
        else if(!GenericTools.imageViewhasImage(imageInserted4))
        {
            currentImageId = R.id.imageInserted_4;
            showDialogTakePicture();
        }


    }

    private void showDialogTakePicture() {
        TakePictureDialog takePictureDialog = new TakePictureDialog(getActivity(),getString(R.string.add_picture),this);
        takePictureDialog.show();
    }

    @OnClick(R.id.addprestation)void  addPrestationClicked()
    {

        ListDialog listDialog = new ListDialog(getActivity(),this);
        listDialog.show();
    }


    @OnClick(R.id.btn_add_signature)void  addSignatureClicked()
    {
        if(mainActivity.methodRequiresSignaturePermission(InterventionFragment.this))
            mainActivity.showSignatureDaialog();
    }

    @Override
    public void takePictureClicked() {

        isTakePicture = true;
        if(GenericTools.checkPermissions(getActivity(),Constant.permissions,Constant.CAMERA_PERMISSIONS,this)) {

            try {
                dispatchTakePictureIntent();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private void dispatchTakePictureIntent() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            if(android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                Uri fileUri = getOutputMediaFileUri();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
            else
            {

                //Uri photoURI = Uri.fromFile(createImageFile());
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        getOutputMediaFile());

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }


            }
    }

    private Uri getOutputMediaFileUri(){
        return Uri.fromFile(getOutputMediaFile());
    }
    private  File getOutputMediaFile(){

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Constant.PICTURE_DIRECTORY);

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("Briconett", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_BRICONETT_ANDROID_"+ timeStamp + Constant.JPEG_EXTENSION);

        fileNameTemp = mediaFile.getName();
        filePathTemp = mediaFile.getAbsolutePath();

        return mediaFile;
    }


    @Override
    public void fromGalleryClicked() {
        isTakePicture = false;
        if(GenericTools.checkPermissions(getActivity(), Constant.permissions,Constant.CAMERA_PERMISSIONS,this)) {
            takePictureInGallery();
        }

    }

    private void takePictureInGallery() {

        Intent galleryIntent  = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent , Constant.RESULT_GALLERY);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK)
        {
            //ResizeImageAsyncTask resizeImageAsyncTask = new ResizeImageAsyncTask(this);

            //resizeImageAsyncTask.execute(filePathTemp, fileNameTemp);
            try {
                resizePicture(filePathTemp,fileNameTemp);
                showImageTaken(filePathTemp);
                getCountImages();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else if(requestCode == Constant.RESULT_GALLERY && resultCode == Activity.RESULT_OK)
        {

            onSelectFromGalleryResult(data);
            getCountImages();

        }

    }

    private void getCountImages() {
        int nbImages = 0;
        if(GenericTools.imageViewhasImage(imageInserted1))
        {
            nbImages ++;
        }
        if(GenericTools.imageViewhasImage(imageInserted2))
        {
            nbImages ++;
        }
        if(GenericTools.imageViewhasImage(imageInserted3))
        {
            nbImages ++;
        }
        if(GenericTools.imageViewhasImage(imageInserted4))
        {
            nbImages ++;
        }
        text_pictures_count.setText("");
      //  text_pictures_count.setText(String.valueOf(nbImages + ((nbImages > 1) ? " photos:" : " photo:")));
        if(nbImages == 0)
        {
            text_pictures_count.setText("Aucune photo");
            containerPhotos.setVisibility(View.GONE);
        }
        else
        {
            containerPhotos.setVisibility(View.VISIBLE);
        }


    }

    private void showImageTaken(String pathImage){

        if(!getActivity().isFinishing()){

            String fileUri = "file:/"+pathImage;

            switch(currentImageId){

                case R.id.imageInserted_1: {
                    ImageLoader.getInstance().displayImage(fileUri, imageInserted1);

                    imageInserted1.setImageBitmap(BitmapFactory.decodeFile(pathImage));

                    layoutPic1.setVisibility(View.VISIBLE);

                    image1 = new File(fileUri.replace("file:/",""));
                    break;
                }

                case R.id.imageInserted_2: {
                    ImageLoader.getInstance().displayImage(fileUri, imageInserted2);

                    imageInserted2.setImageBitmap(BitmapFactory.decodeFile(pathImage));

                    layoutPic2.setVisibility(View.VISIBLE);

                    image2 = new File(fileUri.replace("file:/",""));
                    break;
                }
                case R.id.imageInserted_3: {
                    ImageLoader.getInstance().displayImage(fileUri, imageInserted3);

                    imageInserted3.setImageBitmap(BitmapFactory.decodeFile(pathImage));

                    layoutPic3.setVisibility(View.VISIBLE);

                    image3 = new File(fileUri.replace("file:/",""));
                    break;
                }
                case R.id.imageInserted_4: {
                    ImageLoader.getInstance().displayImage(fileUri, imageInserted4);

                    imageInserted4.setImageBitmap(BitmapFactory.decodeFile(pathImage));

                    layoutPic4.setVisibility(View.VISIBLE);

                    image4 = new File(fileUri.replace("file:/",""));
                    imageInserted1.setMaxWidth(15);
                    imageInserted2.setMaxWidth(15);
                    imageInserted3.setMaxWidth(15);
                    imageInserted4.setMaxWidth(15);
                    break;
                }
                default : break ;
            }
        }


    }


    private void resizePicture(String pathfile,String fileName) throws IOException
    {

        try {

            int PICTURE_SIZE = 320;

            Bitmap bitmapDecoded = GenericTools.decodeSampledBitmapFromUri(pathfile, PICTURE_SIZE, PICTURE_SIZE);

            Bitmap bitmap = GenericTools.rotateImage(bitmapDecoded,pathfile);

            if(bitmap==null)
                bitmap = bitmapDecoded;

            savebitmap(bitmap,fileName);

        }catch (OutOfMemoryError outOfMemoryError){

            outOfMemoryError.printStackTrace();
        }

    }
    public static void savebitmap(Bitmap bmp,String filename) throws IOException {

        if(bmp!=null)
        {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+File.separator+ Constant.PICTURE_DIRECTORY,filename);

            f.createNewFile();

            FileOutputStream fo = new FileOutputStream(f);

            fo.write(bytes.toByteArray());

            fo.close();

        }

    }
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null)
        {
            try
            {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        switch(currentImageId){

            case R.id.imageInserted_1: {
                imageInserted1.setImageBitmap(bm);

                Uri selectedImage = data.getData();
                image1 = new File(getPath(selectedImage));

                layoutPic1.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.imageInserted_2: {
                imageInserted2.setImageBitmap(bm);

                Uri selectedImage = data.getData();
                image2 = new File(getPath(selectedImage));

                layoutPic2.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.imageInserted_4: {
                imageInserted4.setImageBitmap(bm);

                Uri selectedImage = data.getData();
                image4 = new File(getPath(selectedImage));

                layoutPic4.setVisibility(View.VISIBLE);
                imageInserted1.setMaxWidth(15);
                imageInserted2.setMaxWidth(15);
                imageInserted3.setMaxWidth(15);
                imageInserted4.setMaxWidth(15);
                break;
            }
            case R.id.imageInserted_3: {
                imageInserted3.setImageBitmap(bm);

                Uri selectedImage = data.getData();
                image3 = new File(getPath(selectedImage));

                layoutPic3.setVisibility(View.VISIBLE);

                break;
            }
            default : break ;
        }
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index =             cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }


    @OnClick(R.id.imageInserted_1)void onImage1Clicked()
    {
        currentImageId = R.id.imageInserted_1;
        showDialogTakePicture();
    }

    @OnClick(R.id.imageInserted_2)void onImage2Clicked()
    {
        currentImageId = R.id.imageInserted_2;
        showDialogTakePicture();
    }
    @OnClick(R.id.imageInserted_3)void onImage3Clicked()
    {
        currentImageId = R.id.imageInserted_3;
        showDialogTakePicture();
    }
    @OnClick(R.id.imageInserted_4)void onImage4Clicked()
    {
        currentImageId = R.id.imageInserted_4;
        showDialogTakePicture();
    }

    private void initCommentList() {
        commentAdapter = new CommentAdapter(getActivity(),MainActivity.comments);
        liste_comment.setAdapter(commentAdapter);
        liste_comment.setVisibility(View.VISIBLE);
        GenericTools.setListViewHeightBasedOnChildren(liste_comment,getActivity());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case Constant.CAMERA_PERMISSIONS:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                    if (isTakePicture)
                    {
                        try {
                            dispatchTakePictureIntent();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    else
                    {
                        takePictureInGallery();
                    }
                } else {
                    Toast.makeText(getActivity(), "permission obligatoire", Toast.LENGTH_SHORT).show();
                }
                return;

            default:
                break;
        }
    }




    @Override
    public void resizeFinished(String filePath) {
        if (!GenericTools.isNullOrEmpty(filePath)){
            String fileUri = "file:/"+ filePath;

            if (imageInserted1 != null) {
                imageInserted1.setLayoutParams(new ViewGroup.LayoutParams(30,30));
                ImageLoader.getInstance().displayImage(fileUri, imageInserted1,new ImageSize(30,30));
            }

        }else {
            //file null
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.map = googleMap;


        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){

            map.setMyLocationEnabled(false);

//            map.getUiSettings().setMapToolbarEnabled(true);

        } else {

            //todo  show message permission required

            FragmentManager fm = getChildFragmentManager();

            fm.popBackStack();



        }


        GPSTrackerService gpsTracker = new GPSTrackerService(getActivity());

        Location location = gpsTracker.location;

        if (location != null) {



            //aeroport ivato test
            //double latitude = -18.799068;
            //double longitude = 47.475014;
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            myPosition = new LatLng(latitude, longitude);
           // InfoWindowMap infoWindowMap_my_position = new InfoWindowMap();
           // infoWindowMap_my_position.setCode(1);
           // map.setInfoWindowAdapter(new CustomInfoWindow(_mainActivity));

            Marker my_position = map.addMarker(new MarkerOptions()
                    .position(myPosition)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)));


            //my_position.setData(infoWindowMap_my_position);
            my_position.showInfoWindow();


            CameraPosition cameraPosition = new CameraPosition.Builder().target(myPosition)
                    .zoom(15)
                    .bearing(0)
                    .tilt(45)
                    .build();

            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        }

    }

    private void createMapFragment() {

        FragmentManager fm = getChildFragmentManager();

        mapFragment = createInstanceMapFragment();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.add(R.id.map, mapFragment);

        fragmentTransaction.commit();

        mapFragment.getExtendedMapAsync(this);

    }

    protected SupportMapFragment createInstanceMapFragment() {

        return SupportMapFragment.newInstance();

    }

    @Override
    public void cancel() {

    }


    //MOX
    @Override
    public void addPrestation() {
        getListPrestationSelected();

    }


    @Override
    public void refreshList() {
        getListPrestationSelected();
    }

    @Override
    public void onPermissionsGranted() {
        mainActivity.showSignatureDaialog();
    }

    @Override
    public void imageSelected(Uri selectedImage) {

    }

    @Override
    public void imageTacked(File selectedImage) {

    }

    @Override
    public void saveFile(File selectedImage) {

        String picturePath = selectedImage.getAbsolutePath();

        Bitmap bm = GenericTools.decodeSampledBitmapFromUri(picturePath, Constant.SIGNATURE_PICTUR_WIDTH, Constant.SIGNATURE_PICTUR_HEIGTH);

        imageSignature.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageSignature.setImageBitmap(bm);

        imageSign = selectedImage;
        layoutSign.setVisibility(View.VISIBLE);


    }

}
