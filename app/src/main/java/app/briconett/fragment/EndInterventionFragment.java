package app.briconett.fragment;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import app.briconett.R;
import app.briconett.activity.MainActivity;
import app.briconett.base.BaseFragment;
import app.briconett.dialogs.MessageDialog;
import app.briconett.models.Client;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.Constant;
import app.briconett.tools.GPSTrackerService;
import app.briconett.tools.GenericTools;
import app.briconett.webservice.AccessClientRequest;
import app.briconett.webservice.AddEvenementRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class EndInterventionFragment extends BaseFragment  implements ZXingScannerView.ResultHandler{

    @BindView(R.id.containerView)LinearLayout containerView;
    ZXingScannerView mScannerView;
    @BindView(R.id.tempsPasse)TextView tempsPasse;
    @BindView(R.id.tempsEstime)TextView tempsEstime;
    private static CheckBox checkBoxAction;
    private static EditText txtDescriptionAC;
    private static java.io.File image1;
    private static File image2;
    private static File image3;
    private static File image4;
    private static File imageSign;
    ArrayList<String> listPrestationId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static EndInterventionFragment newInstance() {
        return new EndInterventionFragment();
    }
    public static EndInterventionFragment newInstance( CheckBox _checkBoxAction, EditText _txtDescriptionAC, java.io.File _image1, File _image2, File _image3,File _image4, File _imageSign, ArrayList<String> _listPrestationId
    ) {
        checkBoxAction = _checkBoxAction;
        txtDescriptionAC = _txtDescriptionAC;
        image1 = _image1;
        image2 = _image2;
        image3 = _image3;
        image4 = _image4;
        imageSign = _imageSign;
        return new EndInterventionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_end_intervention, container, false);
        ButterKnife.bind(this, view);
        mScannerView= (ZXingScannerView) view.findViewById(R.id.zxscan2);
        //tempsPasse.setText("");
        tempsEstime.setText("Temps estimé: "+AgentSingleton.getInstance().heurreEstimer);
        //calcul durré intervention
        //milliseconds
        long different = new Date().getTime() - AgentSingleton.getInstance().beginDate.getTime();

        String diffString = String.format("%02dh%02dm%02d",
                TimeUnit.MILLISECONDS.toHours(different),
                TimeUnit.MILLISECONDS.toMinutes(different) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(different)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(different) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(different)));
        //split
        diffString = GenericTools.splitdate(diffString);
        AgentSingleton.getInstance().heurrePasser = diffString;
        tempsPasse.setText("Temps passé: "+AgentSingleton.getInstance().heurrePasser);

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        ((MainActivity)getActivity()).currentFragment = this;
    }

    @Override
    public void handleResult(Result rawResult) {

        mainActivity.showProgress();
        AccessClientRequest accessClientRequest = new AccessClientRequest(getActivity(), new AccessClientRequest.IAccessClientRequestListener() {
            @Override
            public void onAccessClientRequestSucces(Client client) {

                //mainActivity.dismissProgress();


                if (client != null){
                    //todo a enlever juste pour tester
                    //client.setLattitude(48.8534);
                    //client.setLongitude(2.3488);

                    if (AgentSingleton.getInstance().isEndScanQRC){

                        AgentSingleton.getInstance().isEndScanQRC = false;


                        if(client.getLattitude() == null || client.getLongitude()== null )
                        {
                            mainActivity.dismissProgress();
                            MessageDialog messageDialog = new MessageDialog(getContext(), "", "Prestation bien enregistrée", true, new MessageDialog.IMessageDialogListener() {
                                @Override
                                public void messageDialogClicked() {
                                    enregistrer();
                                    AgentSingleton.getInstance().interv = null;
                                    mainActivity.pushFragment(HomeFragment.newInstance());

                                }
                            });
                            messageDialog.show();
                            return;
                        }
                        GPSTrackerService gpsTrackerService = new GPSTrackerService(getContext());
                        Location location = gpsTrackerService.getLocation();
                        if(location != null)
                        {
                            String clientLat = client.getLattitude().toString();
                            String clientLng = client.getLongitude().toString();
                            float[] distance = new float[1];
                            Location.distanceBetween(Double.valueOf(clientLat), Double.valueOf(clientLng), location.getLatitude(), location.getLongitude(), distance);
                            // distance[0] is now the distance between these lat/lons in meters
                            if (distance[0] < 500) {

                                mainActivity.dismissProgress();

                                MessageDialog messageDialog = new MessageDialog(getContext(), "", "Prestation bien enregistrée", true, new MessageDialog.IMessageDialogListener() {
                                    @Override
                                    public void messageDialogClicked() {
                                        enregistrer();
                                        AgentSingleton.getInstance().interv = null;
                                        mainActivity.pushFragment(HomeFragment.newInstance());

                                    }
                                });
                                messageDialog.show();
                            }
                            else {
                                mainActivity.dismissProgress();
                                MessageDialog messageDialog = new MessageDialog(getContext(), "Attention", "Vous n’êtes plus sur place, veuillez la prochaine fois clôturer votre prestation sur place.", true, new MessageDialog.IMessageDialogListener() {
                                    @Override
                                    public void messageDialogClicked() {
                                        enregistrer();
                                        AgentSingleton.getInstance().interv = null;
                                    }
                                });
                                messageDialog.show();
                            }
                        }
                    }
                    else
                    {

                        AgentSingleton.getInstance().client = client;

                        GenericTools.hiddenKeyboard(getActivity());
                        mainActivity.pushFragment(InterventionFragment.newInstance());
                        mainActivity.dismissProgress();

                    }
                    mScannerView.resumeCameraPreview(EndInterventionFragment.this);

                }

            }

            @Override
            public void onAccessClientRequestError(String message) {

                mainActivity.dismissProgress();

                Toast.makeText(getActivity(), "Code client incorrecte !", Toast.LENGTH_SHORT).show();

            }
        });
        accessClientRequest.execute(rawResult.getText());
        //accessClientRequest.execute("NTg1");





    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case Constant.CAMERA_PERMISSIONS:

                new Handler().post(new Runnable() {
                    public void run() {
                        mainActivity.pushFragment(new ScanFragment());
                    }
                });
            default:
                break;
        }
    }

   /* @OnClick(R.id.btnLink2) void onbtnLink2Clicked()
    {
        //todo call ws send geolocalisation
        AgentSingleton.getInstance().isEndScanQRC = false;
        mainActivity.pushFragment(new HomeFragment());
    }*/


    @OnClick(R.id.btnLink2) void onbtnLink3Clicked()
    {
        AgentSingleton.getInstance().isEndScanQRC = false;

        MessageDialog messageDialog = new MessageDialog(getContext(), "Attention", "Vous n’êtes plus sur place, veuillez la prochaine fois clôturer votre prestation sur place.", true, new MessageDialog.IMessageDialogListener() {
       @Override
            public void messageDialogClicked() {
           enregistrer();
           AgentSingleton.getInstance().interv = null;
           AgentSingleton.getInstance().beginDate = null;
           AgentSingleton.getInstance().heurreEstimer = null;
           AgentSingleton.getInstance().debut = null;
           mainActivity.pushFragment(new HomeFragment());
            }
        });
        messageDialog.show();
    }

    @OnClick(R.id.containerView) void onContainerClicked()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    /*@OnClick(R.id.btnScan) void onFabClicked()
    {
        if (GenericTools.checkPermissions(getActivity(),Constant.permissions,Constant.CAMERA_PERMISSIONS,this)) {
            mainActivity.pushFragment(new ScanFragment());
        }
    }*/
    @OnClick(R.id.rev) void onbtnRevClicked()
    {
        mainActivity.pushFragment(InterventionFragment.newInstance(checkBoxAction, txtDescriptionAC, image1, image2, image3,image4, imageSign, listPrestationId));
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.removeAllViews(); //<- here remove all the views, it will make an Activity having no View
        mScannerView.stopCamera();
    }

    public void enregistrer(){

        AgentSingleton.getInstance().isEndScanQRC = true;

        //mainActivity.pushFragment(EndInterventionFragment.newInstance());

       // getIdPrestationSelected();
        mainActivity.showProgress();
        AddEvenementRequest addEvenementRequest = new AddEvenementRequest(getActivity(), new AddEvenementRequest.IAddEvenementRequestListener() {
            @Override
            public void addEventSucces() {

                mainActivity.dismissProgress();
                GenericTools.reinitPrestationList();
                mainActivity.pushFragment(HomeFragment.newInstance());

            }

            @Override
            public void addEventError(String message) {
                mainActivity.dismissProgress();
            }
        });



        Date cDate = new Date();
        String fDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);
        String fDatefin = new SimpleDateFormat("HH:mm").format(cDate);
        boolean isSurPlace = getLocationUser();

        addEvenementRequest.execute(
                isSurPlace,
                fDate,
                AgentSingleton.getInstance().interv.getTitre(),
                "",
                (checkBoxAction.isChecked()) ? true : false,
                txtDescriptionAC.getText().toString(),
                AgentSingleton.getInstance().client.getIdClient(),
                AgentSingleton.getInstance().currentUser.getId(),
                image1,
                image2,
                image3,
                image4,
                imageSign,
                listPrestationId,
                AgentSingleton.getInstance().client.getNom(),
                AgentSingleton.getInstance().heurreEstimer,
                AgentSingleton.getInstance().heurrePasser,"",
                AgentSingleton.getInstance().debut,
                fDatefin
        );
        AgentSingleton.getInstance().debut = null;
        AgentSingleton.getInstance().beginDate = null;

    }
    private boolean getLocationUser() {

        GPSTrackerService gpsTrackerService = new GPSTrackerService(getContext());
        Location location = gpsTrackerService.getLocation();
        Client client = AgentSingleton.getInstance().client;
        if(location != null && client != null )
        {
            String clientLat = client.getLattitude().toString();
            String clientLng = client.getLongitude().toString();
            float[] distance = new float[1];
            Location.distanceBetween(Double.valueOf(clientLat), Double.valueOf(clientLng), location.getLatitude(), location.getLongitude(), distance);
            // distance[0] is now the distance between these lat/lons in meters
            if (distance[0] < 500)
            {
                return true;
            }
        }
        return false;
    }



}
