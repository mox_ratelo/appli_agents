package app.briconett.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import app.briconett.R;
import app.briconett.activity.MainActivity;


import app.briconett.adapter.PrestationDetailAdapter;
import app.briconett.base.BaseFragment;
import app.briconett.models.EventModel;
import app.briconett.models.ImageEvent;
import app.briconett.models.Intervention;
import app.briconett.tools.GenericTools;

import static app.briconett.activity.MainActivity.prest;

public class DetailInterventionFragment extends BaseFragment {
    EventModel eventModel;
    TextView nomclient;
    TextView datein;
    TextView heuredebut;
    TextView heurefin;
    TextView dureeestimee;
    TextView tempspasse;
    TextView txtCommentaire;
    TextView txtphotoEmpty;
    TextView txtSignatureEmpty;
    ListView listprestation;
    ImageView imgSignature;
    ImageView imageInserted_12;
    ImageView imageInserted_22;
    ImageView imageInserted_32;
    ImageView imageInserted_42;
    PrestationDetailAdapter adapter;
    ImageEvent imageEvent1;
    ImageEvent imageEvent2;
    ImageEvent imageEvent3;
    ImageEvent imageEvent4;
    ImageEvent imageSignature;
    LinearLayout containerPhotos;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static DetailInterventionFragment newInstance(EventModel eventModel) {

          DetailInterventionFragment detailInterventionFragment = new  DetailInterventionFragment();
          detailInterventionFragment.eventModel = eventModel;
          return detailInterventionFragment;
    }


    public DetailInterventionFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_intervention, container, false);
        nomclient = (TextView) view.findViewById(R.id.nomcli);
        datein = (TextView) view.findViewById(R.id.dateint);
        heuredebut = (TextView) view.findViewById(R.id.heuredeb);
        heurefin = (TextView) view.findViewById(R.id.heurefin);
        dureeestimee = (TextView) view.findViewById(R.id.dureeestimee);
        tempspasse = (TextView) view.findViewById(R.id.valeurtempspasse);
        txtCommentaire = (TextView) view.findViewById(R.id.txtCommentaire);
        txtphotoEmpty = (TextView) view.findViewById(R.id.txtphotoEmpty);
        txtSignatureEmpty = (TextView) view.findViewById(R.id.txtSignatureEmpty);
        listprestation = (ListView) view.findViewById(R.id.listprestation2);
        imgSignature = (ImageView) view.findViewById(R.id.imgSignature);
        imageInserted_12 = (ImageView) view.findViewById(R.id.imageInserted_12);
        imageInserted_22 = (ImageView) view.findViewById(R.id.imageInserted_22);
        imageInserted_32 = (ImageView) view.findViewById(R.id.imageInserted_32);
        imageInserted_42 = (ImageView) view.findViewById(R.id.imageInserted_42);
        containerPhotos = (LinearLayout) view.findViewById(R.id.containerPhotos);
        if (eventModel != null) {
            if (!GenericTools.isNullOrEmpty(eventModel.getSignature_client())) {
                nomclient.setText(eventModel.getSignature_client());
            }
            else {
                nomclient.setVisibility(View.GONE);
            }
            if (!GenericTools.isNullOrEmpty(eventModel.getDate())) {
                datein.setText("Date intervention : "+eventModel.getDate());
            }
            if (!GenericTools.isNullOrEmpty(eventModel.getHeure_debut()))
            {
                heuredebut.setText("Heure début : "+eventModel.getHeure_debut());
            }
            if (!GenericTools.isNullOrEmpty(eventModel.getHeure_fin()))
            {
                heurefin.setText("Heure fin : "+eventModel.getHeure_fin());
            }
            if (!GenericTools.isNullOrEmpty(eventModel.getDureeprevue()))
            {
                dureeestimee.setText("Durée estimée : "+ eventModel.getDureeprevue());
            }
            if (!GenericTools.isNullOrEmpty(eventModel.getDureeeffectuee()))
            {
                tempspasse.setText("Temps passé : "+ GenericTools.splitdate(eventModel.getDureeeffectuee()));
            }
            if (!GenericTools.isNullOrEmpty(eventModel.getCommentaire()))
            {
                txtCommentaire.setText(eventModel.getCommentaire());
            }
            else
            {
                txtCommentaire.setText("Aucun commentaire");
            }
            if(eventModel.getPrestations() != null)
            {
                adapter = new PrestationDetailAdapter(eventModel.getPrestations(),getActivity());
                listprestation.setAdapter(adapter);
                GenericTools.setListViewHeightBasedOnChildren(listprestation,getActivity());
            }
            boolean hasPhotos = false;
            boolean hasSignaturPhotos = false;
            if(eventModel.getImage_evenements() != null)
            {
                ((MainActivity)getActivity()).showProgressDialog();
                if(eventModel.getImage_signature()!=null && !eventModel.getImage_signature().isEmpty() ){
                    if(eventModel.getImage_signature().get(0)!=null) {
                        ImageLoader.getInstance().displayImage("https://client-briconett.fr/import/img_evn/" + eventModel.getId() + "/" + eventModel.getImage_signature().get(0).getImg_file_name(), imgSignature);
                        hasSignaturPhotos = true;
                    }
                }
                for(ImageEvent imageEvent : eventModel.getImage_evenements())
                {
                    String url = "https://client-briconett.fr/import/img_evn/"+eventModel.getId()+"/"+imageEvent.getImg_file_name();
                  /*  if(imageEvent.getImg_file_name().contains("signature"))
                    {
                        ImageLoader.getInstance().displayImage(url, imgSignature);
                        hasPhotos = true;
                    }*/

                    if(imageEvent1== null)
                    {
                        hasPhotos = true;
                        imageEvent1 = imageEvent;
                        imageInserted_12.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(url, imageInserted_12);

                    }
                    else if(imageEvent2== null)
                    {
                        hasPhotos = true;
                        imageEvent2 = imageEvent;
                        imageInserted_22.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(url, imageInserted_22);

                    }
                    else if(imageEvent3== null)
                    {
                        hasPhotos = true;
                        imageEvent3 = imageEvent;
                        imageInserted_32.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(url, imageInserted_32);

                    }
                    else if(imageEvent4== null)
                    {
                        hasPhotos = true;
                        imageEvent4 = imageEvent;
                        imageInserted_42.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(url, imageInserted_42);

                    }

                }

                ((MainActivity)getActivity()).dismissProgressDialog();

            }
            if(!hasPhotos)
            {
                txtphotoEmpty.setVisibility(View.VISIBLE);
            } if(!hasSignaturPhotos)
            {
                txtSignatureEmpty.setVisibility(View.VISIBLE);
            }
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
     }


}
