package app.briconett.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.briconett.activity.MainActivity;
import app.briconett.R;
import app.briconett.base.BaseFragment;
import app.briconett.models.Client;
import app.briconett.models.User;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.GenericTools;
import app.briconett.webservice.GetListClientRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.saisie)LinearLayout saisie;
    @BindView(R.id.listeinterventions)LinearLayout liste;
    @BindView(R.id.nom)TextView nom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        Intent i = this.getActivity().getIntent();
        User user = AgentSingleton.getInstance().currentUser;

        String name = "";
        if (user != null && !GenericTools.isNullOrEmpty(user.getNom())) {
            name = name+ user.getNom();
            if(!GenericTools.isNullOrEmpty(user.getPrenom()))
            {
                name = name + " "+ user.getPrenom();
            }
            nom.setText(name);
        }
        saisie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).pushFragment(IdentificationInterventionFragment.newInstance());
            }
        });
        liste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).pushFragment(ListInterventionFragment.newInstance());
            }
        });

        //getListeClient();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).currentFragment = this;

    }


    private void getListeClient(){

        mainActivity.showProgress();

        GetListClientRequest getListClientRequest = new GetListClientRequest(AgentSingleton.getInstance().activity, new GetListClientRequest.IGetListClientRequestListener() {
            @Override
            public void onGetListClientRequestSucces(ArrayList<Client> clients) {

                AgentSingleton.getInstance().listClients = clients;

                new saveClientDBAsyncTask().execute(clients);


            }

            @Override
            public void onGetListClientRequestError(String message) {

                mainActivity.dismissProgress();

            }
        });

        getListClientRequest.execute();

    }



    private class saveClientDBAsyncTask extends AsyncTask<ArrayList<Client>, Void, ArrayList<Client>> {

        @Override
        protected ArrayList<Client> doInBackground(ArrayList<Client>... params)
        {

            ArrayList<Client> arrayList = params[0];

            return arrayList;

        }

        @Override
        protected void onPostExecute(ArrayList<Client> produitDBs)
        {

            mainActivity.dismissProgress();

        }

    }



}