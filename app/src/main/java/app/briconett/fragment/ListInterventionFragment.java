package app.briconett.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import app.briconett.R;
import app.briconett.activity.MainActivity;

import app.briconett.adapter.InterventionAdapter;
import app.briconett.base.BaseFragment;
import app.briconett.models.EventModel;
import app.briconett.models.Intervention;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.GenericTools;
import app.briconett.webservice.DetailEvenementRequest;
import app.briconett.webservice.GetListEvenementRequest;

import static app.briconett.activity.MainActivity.prest;

public class ListInterventionFragment extends BaseFragment {
    InterventionAdapter adapter;
    ListView listintervention;
    ArrayList<Intervention> interventions;
    ArrayList<EventModel> listEventModels;
    TextView textNoResult;
    TextView aucune;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ListInterventionFragment newInstance() {
        return new ListInterventionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_intervention, container, false);
        listintervention = (ListView)view.findViewById(R.id.listinterv);
        textNoResult = (TextView) view.findViewById(R.id.textNoResult);
        aucune = (TextView) view.findViewById(R.id.aucune);
        listintervention.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //((MainActivity)getActivity()).pushFragment(DetailInterventionFragment.newInstance(interventions.get(i)));

                final EventModel eventModel = listEventModels.get(i);

                //mainActivity.showProgress();

                ((MainActivity)getActivity()).showProgressDialog();
                DetailEvenementRequest detailEvenementRequest = new DetailEvenementRequest(AgentSingleton.getInstance().activity, new DetailEvenementRequest.IDetailEvenementRequestListener() {
                    @Override
                    public void onDetailRequestSucces(EventModel eventModelResult) {


                        //mainActivity.dismissProgress();
                        // Affichage Détail ici ....

                        // Ty ny url azahoana ny image :
                        //  https://client-briconett.fr/import/img_evn/{idIntervention:526}/passage_1514399231.jpg
                        mainActivity.pushFragment(DetailInterventionFragment.newInstance(eventModelResult));
                        ((MainActivity)getActivity()).showProgressDialog();

                    }

                    @Override
                    public void onDetailRequestError(String message) {
                        //mainActivity.dismissProgress();
                        ((MainActivity)getActivity()).dismissProgressDialog();
                    }
                });

                detailEvenementRequest.execute(eventModel.getId());


            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mainActivity.showProgressDialog();
        getEventList();
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getEventList() {
        GetListEvenementRequest getListEvenementRequest = new GetListEvenementRequest(getActivity(), new GetListEvenementRequest.IGetListEvenementRequestListener() {
            @Override
            public void onGetEvenementRequestSucces(ArrayList<EventModel> eventModels) {
                if(eventModels != null)
                {
                    listEventModels = eventModels;
                    adapter = new InterventionAdapter(eventModels,getActivity());
                    listintervention.setAdapter(adapter);
                }else {

                   // textNoResult.setVisibility(View.VISIBLE);
                    listintervention.setVisibility(View.GONE);
                    aucune.setVisibility(View.VISIBLE);

                }
                mainActivity.dismissProgressDialog();
            }

            @Override
            public void onGetEvenementRequestError(String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mainActivity, "Erreur request", Toast.LENGTH_SHORT).show();
                        mainActivity.dismissProgressDialog();
                    }
                });

            }
        });
       getListEvenementRequest.execute();
    }
   /* private void initList() {
        if(interventions==null)interventions = new ArrayList<Intervention>();
        addList();
        adapter = new InterventionAdapter(interventions,getActivity());
        listintervention.setAdapter(adapter);
    }
    private void addList(){
        interventions.add(new Intervention("Date : 04/12/2017 7h20","Client: Michel Dupont","Durée: 45 Mn"));
        interventions.add(new Intervention("Date : 09/12/2017 13h00","Client: Jean Louis","Durée: 20 Mn"));
        interventions.add(new Intervention("Date : 10/12/2017 15h15","Client: Marie Ignace","Durée: 55 Mn"));
        interventions.add(new Intervention("Date : 13/12/2017 8h00","Client: Michel Dupont","Durée: 30 Mn"));
        interventions.add(new Intervention("Date : 15/12/2017 10h53","Client: Paula Chris","Durée: 38 Mn"));
    }*/


}
