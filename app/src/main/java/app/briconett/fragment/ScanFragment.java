package app.briconett.fragment;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.Result;

import java.util.Date;
import java.util.Locale;

import app.briconett.base.BaseFragment;
import app.briconett.dialogs.MessageDialog;
import app.briconett.models.Client;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.GPSTrackerService;
import app.briconett.tools.GenericTools;
import app.briconett.webservice.AccessClientRequest;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Phael on 29/12/2017.
 */

public class ScanFragment extends BaseFragment implements ZXingScannerView.ResultHandler{

    private ZXingScannerView mScannerView;



    public ScanFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mScannerView = new ZXingScannerView(getActivity());
        return mScannerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void handleResult(Result rawResult) {

            mainActivity.showProgress();
            AccessClientRequest accessClientRequest = new AccessClientRequest(getActivity(), new AccessClientRequest.IAccessClientRequestListener() {
                @Override
                public void onAccessClientRequestSucces(Client client) {

                    //mainActivity.dismissProgress();


                    if (client != null){
                        //todo a enlever juste pour tester
                        //client.setLattitude(48.8534);
                        //client.setLongitude(2.3488);

                        if (AgentSingleton.getInstance().isEndScanQRC){

                            AgentSingleton.getInstance().isEndScanQRC = false;


                            if(client.getLattitude() == null || client.getLongitude()== null )
                            {
                                mainActivity.dismissProgress();
                                mainActivity.pushFragment(HomeFragment.newInstance());
                                return;
                            }
                            GPSTrackerService gpsTrackerService = new GPSTrackerService(getContext());
                            Location location = gpsTrackerService.getLocation();
                            if(location != null)
                            {
                                String clientLat = client.getLattitude().toString();
                                String clientLng = client.getLongitude().toString();
                                float[] distance = new float[1];
                                Location.distanceBetween(Double.valueOf(clientLat), Double.valueOf(clientLng), location.getLatitude(), location.getLongitude(), distance);
                                // distance[0] is now the distance between these lat/lons in meters
                                if (distance[0] < 500) {

                                    mainActivity.dismissProgress();

                                    MessageDialog messageDialog = new MessageDialog(getContext(), "", "Prestation bien enregistrée", true, new MessageDialog.IMessageDialogListener() {
                                        @Override
                                        public void messageDialogClicked() {
                                            mainActivity.pushFragment(HomeFragment.newInstance());

                                        }
                                    });
                                    messageDialog.show();
                                }
                                else {
                                    mainActivity.dismissProgress();
                                    MessageDialog messageDialog = new MessageDialog(getContext(), "", "Il faut bien terminer sur place", true, new MessageDialog.IMessageDialogListener() {
                                        @Override
                                        public void messageDialogClicked() {

                                        }
                                    });
                                    messageDialog.show();
                                }
                            }
                        }
                        else
                        {

                            AgentSingleton.getInstance().client = client;

                            GenericTools.hiddenKeyboard(getActivity());
                            mainActivity.pushFragment(InterventionFragment.newInstance());
                            mainActivity.dismissProgress();

                        }
                        mScannerView.resumeCameraPreview(ScanFragment.this);

                    }

                }

                @Override
                public void onAccessClientRequestError(String message) {

                    mainActivity.dismissProgress();

                    Toast.makeText(getActivity(), "Code client incorrecte !", Toast.LENGTH_SHORT).show();

                }
            });
         accessClientRequest.execute(rawResult.getText());
            //accessClientRequest.execute("NTg1");





    }



    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

}
