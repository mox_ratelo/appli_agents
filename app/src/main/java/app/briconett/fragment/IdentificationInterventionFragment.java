package app.briconett.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import java.io.IOException;
import java.util.Date;


import app.briconett.BuildConfig;
import app.briconett.activity.MainActivity;
import app.briconett.R;
import app.briconett.activity.SimpleScannerActivity;
import app.briconett.base.BaseFragment;
import app.briconett.dialogs.MessageDialog;
import app.briconett.models.Client;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.Constant;
import app.briconett.tools.GPSTrackerService;
import app.briconett.tools.GenericTools;
import app.briconett.webservice.AccessClientRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class IdentificationInterventionFragment extends BaseFragment  implements ZXingScannerView.ResultHandler{

    @BindView(R.id.btnIdentification)Button btnIdentification;
    @BindView(R.id.codeclient)EditText editCode;
    @BindView(R.id.containerView)LinearLayout containerView;
  //  @BindView(R.id.fab)FloatingActionButton fab;
    ZXingScannerView mScannerView1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static IdentificationInterventionFragment newInstance() {
        return new IdentificationInterventionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_identification_intervention, container, false);
        ButterKnife.bind(this, view);
        mScannerView1= (ZXingScannerView) view.findViewById(R.id.zxscan);
        /* if (BuildConfig.DEBUG) {
            code.setText("NTg1");
        }
*/
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView1.setResultHandler(this);
        mScannerView1.startCamera();
        ((MainActivity)getActivity()).currentFragment = this;
    }
    @Override
    public void handleResult(Result rawResult) {

        mainActivity.showProgress();
        AccessClientRequest accessClientRequest = new AccessClientRequest(getActivity(), new AccessClientRequest.IAccessClientRequestListener() {
            @Override
            public void onAccessClientRequestSucces(Client client) {

                //mainActivity.dismissProgress();


                if (client != null){
                    //todo a enlever juste pour tester
                    //client.setLattitude(48.8534);
                    //client.setLongitude(2.3488);

                    if (AgentSingleton.getInstance().isEndScanQRC){

                        AgentSingleton.getInstance().isEndScanQRC = false;


                        if(client.getLattitude() == null || client.getLongitude()== null )
                        {
                            mainActivity.dismissProgress();
                            mainActivity.pushFragment(HomeFragment.newInstance());
                            return;
                        }
                        GPSTrackerService gpsTrackerService = new GPSTrackerService(getContext());
                        Location location = gpsTrackerService.getLocation();
                        if(location != null)
                        {
                            String clientLat = client.getLattitude().toString();
                            String clientLng = client.getLongitude().toString();
                            float[] distance = new float[1];
                            Location.distanceBetween(Double.valueOf(clientLat), Double.valueOf(clientLng), location.getLatitude(), location.getLongitude(), distance);
                            // distance[0] is now the distance between these lat/lons in meters
                            if (distance[0] < 500) {

                                mainActivity.dismissProgress();

                                MessageDialog messageDialog = new MessageDialog(getContext(), "", "Prestation bien enregistrée", true, new MessageDialog.IMessageDialogListener() {
                                    @Override
                                    public void messageDialogClicked() {
                                        mainActivity.pushFragment(HomeFragment.newInstance());

                                    }
                                });
                                messageDialog.show();
                            }
                            else {
                                mainActivity.dismissProgress();
                                MessageDialog messageDialog = new MessageDialog(getContext(), "Attention", "Vous n’êtes plus sur place, veuillez la prochaine fois clôturer votre prestation sur place.", true, new MessageDialog.IMessageDialogListener() {
                                    @Override
                                    public void messageDialogClicked() {

                                    }
                                });
                                messageDialog.show();
                            }
                        }
                        else
                        {
                            mainActivity.dismissProgress();
                        }
                    }
                    else
                    {

                        AgentSingleton.getInstance().client = client;

                        GenericTools.hiddenKeyboard(getActivity());
                        mainActivity.pushFragment(InterventionFragment.newInstance());
                        mainActivity.dismissProgress();

                    }
                    mScannerView1.resumeCameraPreview(IdentificationInterventionFragment.this);

                }

            }

            @Override
            public void onAccessClientRequestError(String message) {

                mainActivity.dismissProgress();

                Toast.makeText(getActivity(), "Code client incorrecte !", Toast.LENGTH_SHORT).show();

            }
        });
        accessClientRequest.execute(rawResult.getText());

       // accessClientRequest.execute("NTg1");
    }


    @OnClick(R.id.btnIdentification)void onBtnIdentificationClicked()
    {
        if(!GenericTools.isNullOrEmpty(editCode.getText().toString())) {

            mainActivity.showProgress();
            AccessClientRequest accessClientRequest = new AccessClientRequest(getActivity(), new AccessClientRequest.IAccessClientRequestListener() {
                @Override
                public void onAccessClientRequestSucces(Client client) {

                    mainActivity.dismissProgress();

                    if (client != null){

                        AgentSingleton.getInstance().client = client;

                        GenericTools.hiddenKeyboard(getActivity());
                        mainActivity.pushFragment(InterventionFragment.newInstance());

                    }

                }

                @Override
                public void onAccessClientRequestError(String message) {

                    mainActivity.dismissProgress();

                    Toast.makeText(getActivity(), "Code client incorrecte !", Toast.LENGTH_SHORT).show();

                }
            });

            accessClientRequest.execute(editCode.getText().toString());
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case Constant.CAMERA_PERMISSIONS:

                new Handler().post(new Runnable() {
                    public void run() {
                        mainActivity.pushFragment(new IdentificationInterventionFragment());
                       // mainActivity.pushFragment(new ScanFragment());
                    }
                });
            default:
                break;
        }
    }


    @OnClick(R.id.containerView) void onContainerClicked()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

   /* @OnClick(R.id.fab) void onFabClicked()
    {
        if (GenericTools.checkPermissions(getActivity(),Constant.permissions,Constant.CAMERA_PERMISSIONS,this)) {
            mainActivity.pushFragment(new ScanFragment());
        }
    }
*/

    @Override
    public void onPause() {
        super.onPause();
        mScannerView1.removeAllViews(); //<- here remove all the views, it will make an Activity having no View
        mScannerView1.stopCamera();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView1.removeAllViews(); //<- here remove all the views, it will make an Activity having no View
        mScannerView1.stopCamera();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mScannerView1.removeAllViews(); //<- here remove all the views, it will make an Activity having no View
        mScannerView1.stopCamera();
    }
}
