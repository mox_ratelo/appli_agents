package app.briconett.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.briconett.R;
import app.briconett.activity.MainActivity;
import app.briconett.base.BaseFragment;
import app.briconett.dialogs.MessageDialog;
import app.briconett.dialogs.UpadePwdDialog;
import app.briconett.models.User;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.GenericTools;
import app.briconett.webservice.EditPasswordRequest;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phael on 10/01/2018.
 */

public class AccountFragment extends BaseFragment {

    @BindView(R.id.txtName)TextView txtName;
    @BindView(R.id.txtFirstName)TextView txtFirstName;
    @BindView(R.id.txtEmail)TextView txtEmail;
    @BindView(R.id.textPassword)TextView textPassword;
    @BindView(R.id.containerPassword)LinearLayout containerPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static AccountFragment newInstance() {
        return new AccountFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        User user =AgentSingleton.getInstance().currentUser;
        if(user != null)
        {
            if(!GenericTools.isNullOrEmpty(user.getNom()))
            {
                txtName.setText(user.getNom());
            }
            if(!GenericTools.isNullOrEmpty(user.getPrenom()))
            {
                txtFirstName.setText(user.getPrenom());
            }
            if(!GenericTools.isNullOrEmpty(user.getEmail()))
            {
                txtEmail.setText(user.getEmail());
            }
        }
    }

    @OnClick(R.id.btnEditPwd) void onBtnEditClicked()
    {
        showPopupUdate();
    }
    @OnClick(R.id.containerPassword) void onContainerPwdClicked()
    {
        showPopupUdate();
    }

    private void showPopupUdate() {
        UpadePwdDialog upadePwdDialog = new UpadePwdDialog(getActivity(), new UpadePwdDialog.IUpdatePasswordListener() {
            @Override
            public void updatePassword(String password) {

                ((MainActivity)getActivity()).showProgressDialog();
                EditPasswordRequest editPasswordRequest  = new EditPasswordRequest(getActivity(), new EditPasswordRequest.IEditPasswordRequestListener() {
                    @Override
                    public void onEditPasswordRequestSucces(final boolean succes) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (succes) {
                                    new MessageDialog(getActivity(),"Information","Mot de passe modifié avec succés",true,null).show();
                                }
                                else
                                {
                                    new MessageDialog(getActivity(),"Error","Veuillez reéssayer",true,null).show();
                                }

                                ((MainActivity)getActivity()).dismissProgressDialog();
                            }
                        });
                    }

                    @Override
                    public void onEditPasswordRequestError(final String message) {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new MessageDialog(getActivity(),"Error",message,true,null).show();
                                ((MainActivity)getActivity()).dismissProgressDialog();


                            }
                        });
                    }
                });
                editPasswordRequest.execute(String.valueOf(AgentSingleton.getInstance().currentUser.getId()),password,password);
            }
        });
        upadePwdDialog.show();
    }


}
