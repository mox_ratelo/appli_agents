package app.briconett.interfaces;

/**
 * Created by RATELOSON Andrianiaina Miora on 31/01/2017.
 */

public interface IPermissionsGranted {
    void onPermissionsGranted();
}
