package app.briconett.interfaces;

import android.net.Uri;

import java.io.File;


public interface IImageSelected {

    void imageSelected(Uri selectedImage);
    void imageTacked(File selectedImage);
    void saveFile(File selectedImage);

}
