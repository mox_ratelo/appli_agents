package app.briconett.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import app.briconett.BuildConfig;

import app.briconett.R;

import app.briconett.base.AbstractBaseActivity;
import app.briconett.components.CustomEditText;
import app.briconett.interfaces.IPermissionsGranted;
import app.briconett.models.EventModel;
import app.briconett.models.Prestation;
import app.briconett.models.User;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.Constant;
import app.briconett.tools.GenericTools;
import app.briconett.webservice.GetListEvenementRequest;
import app.briconett.webservice.GetListPrestationRequest;
import app.briconett.webservice.LoginRequest;
import butterknife.BindView;
import butterknife.OnClick;


public class ConnectionActivity  extends AbstractBaseActivity implements IPermissionsGranted {

    public static final String LOGIN = "LOGIN";
    public static final String PASSWORD = "PASSWORD";
    @BindView(R.id.btn_connexion)Button btConnection;
    @BindView(R.id.identifiant)CustomEditText editLogin;
    @BindView(R.id.mdp)CustomEditText pwd;
    @BindView(R.id.activity_connection)LinearLayout activity_connection;

    @BindView(R.id.relativeLayout)LinearLayout relativeLayout;


    @Override
    protected int getActivityView() {
        return R.layout.activity_connection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            editLogin.setText("agent");
            pwd.setText("12345678");
        }
        String loginInpreference = GenericTools.getString(this,LOGIN);
        String pwdInpreference = GenericTools.getString(this,PASSWORD);
        if(!GenericTools.isNullOrEmpty(loginInpreference) && !GenericTools.isNullOrEmpty(pwdInpreference))
        {
            login(loginInpreference,pwdInpreference);

        }

    }
    @OnClick(R.id.btn_connexion)void onBtnConnextionClicked()
    {
        final String login = editLogin.getText().toString();
        if(GenericTools.isNullOrEmpty(login))
        {
            editLogin.setError(getResources().getString(R.string.msg_empty_mail));
            return;
        }
        final String password = pwd.getText().toString();
        if(GenericTools.isNullOrEmpty(password))
        {
            pwd.setError(getResources().getString(R.string.msg_empty_pwd));
            return;
        }
        if(pwd.getText().toString().length() < 6)
        {
            pwd.setError(getResources().getString(R.string.msg_length_pwd));
            return;
        }

        login(login,password);
    }

    public void login(final String login, final String password)
    {
        showProgressDialog();

        LoginRequest loginRequest = new LoginRequest(this, new LoginRequest.ILoginRequestListener() {
            @Override
            public void loginSucces(User userLogin)
            {
                if (userLogin != null) {
                    GenericTools.saveString(ConnectionActivity.this,LOGIN,login);
                    GenericTools.saveString(ConnectionActivity.this,PASSWORD,password);
                    AgentSingleton.getInstance().currentUser = userLogin;
                    getListPrestation();
                }
                else {

                    editLogin.setError(getResources().getString(R.string.error_login_or_pwd));
                    Toast.makeText(ConnectionActivity.this, getResources().getString(R.string.error_login_or_pwd), Toast.LENGTH_SHORT).show();

                }
                //dismissProgressDialog();
            }

            @Override
            public void onLoginError(String message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        editLogin.setError(getResources().getString(R.string.error_login_or_pwd));
                        //Toast.makeText(ConnectionActivity.this, getResources().getString(R.string.error_login_or_pwd), Toast.LENGTH_SHORT).show();
                        dismissProgressDialog();
                    }
                });
            }
        });
        loginRequest.execute(login,password);
    }
    public void getListPrestation()
    {
        GetListPrestationRequest getListPrestationRequest = new GetListPrestationRequest(this, new GetListPrestationRequest.IGetListPrestationRequestListener()
        {
            @Override
            public void onGetListPrestationRequestSucces(ArrayList<Prestation> eventModels) {
                if(eventModels != null)
                {
                    AgentSingleton.getInstance().prestationArrayList = eventModels;
                    if(GenericTools.checkPermissions(ConnectionActivity.this,Constant.permissions,Constant.CAMERA_PERMISSIONS,null)) {
                        openMainActivity();
                    }
                }
                else
                {
                    Toast.makeText(ConnectionActivity.this, getResources().getString(R.string.empty_prestation), Toast.LENGTH_SHORT).show();

                }
                dismissProgressDialog();
            }

            @Override
            public void onGetListPrestationRequestError(final String message) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ConnectionActivity.this, message, Toast.LENGTH_SHORT).show();
                        dismissProgressDialog();
                    }
                });


            }
        });
        getListPrestationRequest.execute();
    }


    public void openMainActivity()
    {

        Intent i = new Intent(ConnectionActivity.this, MainActivity.class);
        startActivity(i);
    }

    public void showSnackBar(String message)
    {
        Snackbar snackbar = Snackbar
                .make(activity_connection, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @OnClick(R.id.relativeLayout)void  onParentViewClicked()
    {

        InputMethodManager inputMethodManager = (InputMethodManager)  getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case Constant.CAMERA_PERMISSIONS:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                   openMainActivity();
                } else {
                    Toast.makeText(ConnectionActivity.this, "permission obligatoire", Toast.LENGTH_SHORT).show();
                }
                return;

            default:
                break;
        }
    }

    @Override
    public void onPermissionsGranted() {

    }
}