package app.briconett.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


import app.briconett.base.AbstractBaseActivity;
import app.briconett.base.BaseFragment;
import app.briconett.dialogs.MessageDialog;
import app.briconett.fragment.AccountFragment;
import app.briconett.fragment.DetailInterventionFragment;
import app.briconett.fragment.HomeFragment;
import app.briconett.fragment.IdentificationInterventionFragment;
import app.briconett.fragment.ListInterventionFragment;
import app.briconett.fragment.ScanFragment;
import app.briconett.interfaces.IImageSelected;
import app.briconett.R;
import app.briconett.models.User;
import app.briconett.tools.AgentSingleton;
import app.briconett.tools.GenericTools;
import app.briconett.tools.SignatureDialogBuilder;
import butterknife.OnClick;


public class MainActivity extends AbstractBaseActivity implements NavigationView.OnNavigationItemSelectedListener{
    public BaseFragment currentFragment;
    public static ArrayList<String> prest;
    public static ArrayList<String> vitreries = new ArrayList<>();
    public static ArrayList<String> depoussierage = new ArrayList<>();
    public static ArrayList<String> netoyage = new ArrayList<>();
    public static ArrayList<String> comments = new ArrayList<>();


    @Override
    protected int getActivityView() {
        return R.layout.activity_main;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prest = new ArrayList<String>();
        pushFragment(HomeFragment.newInstance());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        ImageView imgLogout = (ImageView) findViewById(R.id.imgLogout);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView imageView = (ImageView)findViewById(R.id.btnRight);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });

        AgentSingleton.getInstance().activity = this;
        User user = AgentSingleton.getInstance().currentUser;


        View headerView = navigationView.getHeaderView(0);
        TextView userName  = (TextView)headerView.findViewById(R.id.userName);

        String name = "";
        if(user != null)
        {
            if (userName != null) {

                if ( !GenericTools.isNullOrEmpty(user.getNom())) {
                    name += user.getNom();

                }
                if ( !GenericTools.isNullOrEmpty(user.getPrenom())) {
                    name += " "+user.getPrenom();
                }
            }
        }
        if(!GenericTools.isNullOrEmpty(name))
        {
            userName.setText(name);
        }

    }

    public void pushFragment(BaseFragment fragment)
    {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_fragment,fragment,fragment.getClass().getName())
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }
    @Override
    public void onBackPressed() {

        //android.app.FragmentManager fragmentManager = getFragmentManager();
        //super.onBackPressed();
        if((getActiveFragment()instanceof ScanFragment) || (getActiveFragment()instanceof DetailInterventionFragment))
        {
            super.onBackPressed();
        }

        return;

    }

    public BaseFragment getActiveFragment() {

        Log.d("BACK STACK ENTRY",String.valueOf(getSupportFragmentManager().getBackStackEntryCount()));
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            Log.d("BACK STACK ENTRY",String.valueOf(getSupportFragmentManager().getBackStackEntryCount()));
            return null;
        }
        String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();

        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    protected void onNewIntent(Intent intent) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_interv) {
            pushFragment(IdentificationInterventionFragment.newInstance());
        } else if (id == R.id.nav_deconnection) {
            logOut();
        }else if (id == R.id.nav_account) {
            pushFragment(AccountFragment.newInstance());
        }
        else if (id == R.id.nav_interv_list) {
            pushFragment(ListInterventionFragment.newInstance());
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    public void logOut() {
        GenericTools.saveString(this,ConnectionActivity.LOGIN,null);
        GenericTools.saveString(this,ConnectionActivity.PASSWORD,null);
        Intent i = new Intent(MainActivity.this, ConnectionActivity.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private IImageSelected iImageSelected;

    public IImageSelected getiImageSelected() {
        return iImageSelected;
    }

    public void setiImageSelected(IImageSelected iImageSelected) {
        this.iImageSelected = iImageSelected;
    }

    public void showSignatureDaialog(){

        new SignatureDialogBuilder()
                .show(this, new SignatureDialogBuilder.SignatureEventListener() {
                    @Override
                    public void onSignatureEntered(File savedFile) {

                        Bitmap bitmap = BitmapFactory.decodeFile(savedFile.getAbsolutePath());

                        bitmap = GenericTools.rotateImage(bitmap,savedFile.getAbsolutePath());

                        try {

                            getiImageSelected().saveFile(GenericTools.getFileFromImage(bitmap,MainActivity.this));

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onSignatureInputCanceled() {
                    }

                    @Override
                    public void onSignatureInputError(Throwable e) {
                    }

                });

    }

    public void showProgress(){
        showProgressDialog();
    }
    public void dismissProgress(){
        dismissProgressDialog();
    }

    public  void initCountDownTimer(long millisInFuture, long countDownInterval)
    {
        final CountDownTimer timer=new CountDownTimer(millisInFuture, countDownInterval) {
            public void onTick(long millisUntilFinished) {
                //int seconds = (int) (millisUntilFinished / 1000) % 60 ;
                //int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
                //int hours   = (int) ((millisUntilFinished / (1000*60*60)) % 24);


            }
            public void onFinish() {
                MessageDialog messageDialog = new MessageDialog(MainActivity.this, "", "", true,null);
                messageDialog.show();
                //Toast.makeText(MainActivity.this, "Oubliez pas de flasher avant votre depart", Toast.LENGTH_SHORT).show();
            }
        };
        timer.start();

    }



}
